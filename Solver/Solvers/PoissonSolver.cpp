

#include "PoissonSolver.h"
#include "goldKernels/goldMultilayerKernel.h"
#include "Helper.h"
#include <time.h> 

#include <cmath>

//---------------------
//--- PoissonSolver ---
//---------------------

void PoissonSolver::Configure(const std::vector<std::pair<DeviceDescriptor, float> > &usage)
{
	if (usage.size() != 1 ||
		usage[0].second != 1.0f ||
		usage[0].first.DeviceType() != DeviceType::CPU ||
		usage[0].first.UnitCount() != 1)
		throw std::runtime_error("NIY");
}

PerfInfo PoissonSolver::Solve(Grid<real> *dst, Grid<real> *f, Grid<int> *mask,
							  CompressedIndices *boundary,
							  CompressedValues<int> *boundaryType,
							  CompressedValues<real> *dirichletValues,
							  CompressedValues<real> *neumannValues,
							  CompressedValues<real> *nxValues,
							  CompressedValues<real> *nyValues,
							  CompressedValues<real> *nzValues,
							  real dh, real eps, size_t maxIter)
{
	//Checking.
	if (!Grid<real>::HaveSameIndices(dst, _buf) ||
		!Grid<real>::HaveSameIndices(dst, f) ||
		!Grid<real>::HaveSameIndices(dst, mask))
		throw std::runtime_error("Grids with different sizes and/or indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!dirichletValues->IsCompatibleWith(boundary) ||
		!neumannValues->IsCompatibleWith(boundary))
		throw std::runtime_error("Broken compressed boundary values");

	if (dst->MemoryType() != MemoryType::Host ||
		f->MemoryType() != MemoryType::Host ||
		mask->MemoryType() != MemoryType::Host)
		throw std::runtime_error("NIY");

	//Preparing.
	Grid<real>**residual;
	Grid<real>**temp;
	///<summary> Working/boundary/dead mask</summary>
	Grid<int>**lMask;
	Region** region;
	Grid<real>** tf;
	CompressedIndices **boundArr;
	CompressedValues<int> **bTypeArr;
	CompressedValues<real> **nVals, **nxVals, **nyVals, **nzVals;


	int div = std::min(std::min(dst->SizeX(), dst->SizeZ()), dst->SizeY()) - 2;
	//int layers = (int)log2(div);
	int layers = (int)(log10((double)div) / log10(2.0));

	region = new Region*[layers + 1];
	residual = new Grid<real>*[layers + 1];
	temp = new Grid<real>*[layers + 1];
	lMask = new Grid<int>*[layers + 1];
	tf = new Grid<real>*[layers + 1];
	boundArr = new CompressedIndices*[layers + 1];
	bTypeArr = new CompressedValues<int>*[layers + 1];
	nVals = new CompressedValues<real>*[layers + 1];
	nxVals = new CompressedValues<real>*[layers + 1];
	nyVals = new CompressedValues<real>*[layers + 1];
	nzVals = new CompressedValues<real>*[layers + 1];

	for (int n = 0; n <= layers; n++){
  		//��������� ������ ���� � �������?
		residual[n] = Grid<real>::Create(MemoryType::Host, 2*(dst->SizeX()-2)/div+2, 2*(dst->SizeY()-2)/div+2, 2*(dst->SizeZ()-2)/div+2); 
		residual[n]->Clear();
		temp[n] = Grid<real>::Create(MemoryType::Host, (dst->SizeX()-2) / div + 2, (dst->SizeY()-2) / div + 2, (dst->SizeZ()-2) / div + 2);
		temp[n]->Clear();
		if (n == layers){
			lMask[layers] = mask;
			tf[n] = f;
			boundArr[layers] = boundary;
			bTypeArr[layers] = boundaryType;
			nVals[layers] = neumannValues;
			nxVals[layers] = nxValues;
			nyVals[layers] = nyValues;
			nzVals[layers] = nzValues;
		}
		else{
			lMask[n] = Grid<int>::Create(MemoryType::Host, (dst->SizeX() - 2) / div + 2, (dst->SizeY() - 2) / div + 2, (dst->SizeZ() - 2) / div + 2);
			tf[n] = Grid<real>::Create(MemoryType::Host, (dst->SizeX() - 2) / div + 2, (dst->SizeY() - 2) / div + 2, (dst->SizeZ() - 2) / div + 2);
		}
		region[n] = new Region(0, 0, 0, (dst->SizeX() - 2) / div + 2, (dst->SizeY() - 2) / div + 2, (dst->SizeZ() - 2) / div + 2);
		div /= 2;
	}

	temp[layers]->Clear();
	_kernel->ApplyBoundary(temp[layers], mask, boundary, boundaryType, 
		dirichletValues, 
		neumannValues, 
		nxValues, nyValues, nzValues, 
		dh, *region[layers]);

	//Helper::GridPrint(temp[layers]);
	//���������� �� �������, ��� �� ����� �������� �� �������� ������ � ��� ����� �������� ��������
	for (int n = layers - 1; n >= 0; n--){
		Helper::SelectiveRestriction<real>(temp[n], temp[n + 1], *region[n], *region[n + 1]);
		Helper::SmartRestriction(lMask[n + 1], boundArr[n + 1], bTypeArr[n + 1],
			nVals[n + 1], nxVals[n + 1], nyVals[n + 1], nzVals[n + 1],
			*region[n + 1],
			lMask[n], &boundArr[n], &bTypeArr[n],
			&nVals[n], &nxVals[n], &nyVals[n], &nzVals[n],
			*region[n]);



		Helper::SelectiveRestriction<real>(tf[n], tf[n + 1], *region[n], *region[n + 1]);
	}
	
	//Solving.
	//��� ���������, ��� �� �� � dh.
	//������ � ������ �������� ���-�� ����� ������� �������, ������� � �������.
	real tdh = dh*(real)pow(2, layers);
	/*for (int n = layers; n <= layers; n++){
		//NextGridInterpolation(temp[n], temp[n - 1], lMask[n], lMask[n - 1], *region[n]);
		//��� ���� � f ���������
		_kernel->DoIteration(temp[n], temp[n], residual, temp, tf[n], 
			boundArr, bTypeArr, 
			nVals, nxVals, nyVals, nzVals,
			tdh, n, lMask, region);
		tdh /= 2;
	}*/
	clock_t start = clock();
	real max;
	Grid<real >* tdst = Grid<real>::Create(MemoryType::Host, (dst->SizeX() - 2) + 2, (dst->SizeY() - 2) + 2, (dst->SizeZ() - 2) + 2);
	tdst->Clear();
	_kernel->ApplyBoundary(tdst, mask, boundary, boundaryType,
		dirichletValues,
		neumannValues,
		nxValues, nyValues, nzValues,
		dh, *region[layers]);
	do {
		_kernel->DoIteration(tdst,temp[layers], residual, temp, tf[layers],
			boundArr, bTypeArr,
			nVals, nxVals, nyVals, nzVals,
			dh, layers, lMask, region);
		
		max = 0;
		size_t count = 0;
		real* ptr = temp[layers]->Pointer();
		for (size_t z = 0; z < region[layers]->SizeZ(); z++)
			for (size_t y = 0; y < region[layers]->SizeY(); y++)
				for (size_t x = 0; x < region[layers]->SizeX(); x++){
					int index = temp[layers]->ComputeIndex(x, y, z);

					if (lMask[layers]->Pointer()[index] == CellType::Working){
						real srs = temp[layers]->Pointer()[index];
						real drs = tdst->Pointer()[index];
						real rx = (real)x / 129 - 0.5;
						real ry = (real)y / 129 - 0.5;
						real rz = (real)z / 129 - 0.5;

						real r2 = rx*rx + ry*ry + rz*rz;
						real l = sqrt(r2);
						real resval = std::abs(drs - 1000 * 0.25*0.25*0.25 / 2 / r2*rz / l)/*(double)(x+y+z)/129)*/;
						if (max < resval)
							max = resval;
						count++;
					}
				}
		
		Grid<real>* tsrs = tdst;
		tdst = temp[layers];
		temp[layers] = tsrs;
	} while (max > eps);
	clock_t end = clock();
	cout << (end - start) / CLOCKS_PER_SEC << endl;
	Grid<real>::Copy(dst, 0, 0, 0, temp[layers], *region[layers]);

	return PerfInfo();

	/*
	//Solving.
	real curEps = (real)0.0;
	size_t curIter = 0;
	do
	{
		Grid<real> *cur = bufs[curBuf];
		Grid<real> *nxt = bufs[(curBuf + 1) % 2];
		curBuf = (curBuf + 1) % 2;

		for (size_t i = 0; i < 5; i++)
		{
			_kernel->DoIteration(nxt, cur, f, dh, mask, wholeGrid);
			_kernel->ApplyBoundary(nxt, mask, boundary, boundaryType, dirichletValues, neumannValues, wholeGrid);
		}
		
		curEps = _kernel->CalculateResidual(cur, nxt, mask, wholeGrid);
		curIter += 1;
	}
	while (curEps > eps && (curIter < maxIter || maxIter == 0));

	//Not solved =(
	if (maxIter != 0 && curIter >= maxIter)
		throw std::runtime_error("Failed to solve Poisson equation within required count of iterations");
	//Solved =)
	else
	{
		if (curIter % 2 == 1)
			Grid<real>::Copy(dst, 0, 0, 0, _buf, wholeGrid);

		return PerfInfo();
	}*/
}

PoissonSolver *PoissonSolver::Create(size_t sizeX, size_t sizeY, size_t sizeZ)
{
	IPoissonKernel *kernel = NULL;
	Grid<real> *buf = NULL;
	
	try
	{
		kernel = new goldMultilayerKernel();
		buf = Grid<real>::Create(MemoryType::Host, sizeX, sizeY, sizeZ);
	}
	catch (std::exception &)
	{
		if (kernel != NULL)
			delete kernel;

		if (buf != NULL)
			delete buf;
		
		throw;
	}

	return new PoissonSolver(kernel, buf);
}

inline void PoissonSolver::NextGridInterpolation(Grid<real>* dstLayer, Grid<real>* srcLayer,
												 Grid<int> *dstMask, Grid<int> *srcMask,
												 Region dstRegion)
{
	real *dst = dstLayer->Pointer();
	real *src = srcLayer->Pointer();
	int *dMsk = dstMask->Pointer();
	size_t pitchY = dstMask->PitchY();
	size_t pitchZ = dstMask->PitchZ();

	for (size_t z = dstRegion.OffsetZ() + 1; z < dstRegion.OffsetZ() + dstRegion.SizeZ() - 1; z++)
		for (size_t y = dstRegion.OffsetY() + 1; y < dstRegion.OffsetY() + dstRegion.SizeY() - 1; y++)
			for (size_t x = dstRegion.OffsetX() + 1; x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
			{
				size_t srcIndex = srcMask->ComputeIndex((x+1) / 2, (y+1) / 2, (z+1) / 2);
				size_t dstIndex = dstMask->ComputeIndex(x, y, z);
				// ���������������, ��� ��� ���������� ����������� dead � working ��������� � working
				if ((CellType::Type)dMsk[dstIndex] == CellType::Working)
				{
					dst[dstIndex] = src[srcIndex];
				}
			}
}

PoissonSolver::~PoissonSolver()
{
	if (_kernel != NULL)
		delete _kernel;
	_kernel = NULL;

	if (_buf != NULL)
		delete _buf;
	_buf = NULL;
}
