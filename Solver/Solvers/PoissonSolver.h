
#pragma once

#include "Common/PerfInfo.h"
#include "Common/Kernels.h"
#include "Common/Devices.h"


class PoissonSolver
{
	private:
		IPoissonKernel *_kernel;
		Grid<real> *_buf;

	private:
		PoissonSolver(IPoissonKernel *kernel, Grid<real> *buf)
			: _kernel(kernel), _buf(buf)
		{ /*nothinf*/ }

		inline PoissonSolver(const PoissonSolver &)
		{ throw std::runtime_error("Ata-ta"); }

		void static inline NextGridInterpolation(Grid<real>* dstLayer, Grid<real>* srcLayer,
						   Grid<int> *dstMask, Grid<int> *srcMask,
			               Region dstRegion);

	public:
		static bool neumannSelector(int value){
			if (value == (int)BoundaryCondition::Neumann)
				return true;
			return false;
		}

		void Configure(const std::vector<std::pair<DeviceDescriptor, float> > &usage);

		PerfInfo Solve(Grid<real> *dst, Grid<real> *f, Grid<int> *mask,
					   CompressedIndices *boundary,
					   CompressedValues<int> *boundaryType,
					   CompressedValues<real> *dirichletValues,
					   CompressedValues<real> *neumannValues,
					   CompressedValues<real> *nxValues,
					   CompressedValues<real> *nyValues,
					   CompressedValues<real> *nzValues,
					   real dh, real eps, size_t maxIter = 0);

		~PoissonSolver();

		static PoissonSolver *Create(size_t sizeX, size_t sizeY, size_t sizeZ);
};
