
#pragma once

#include "Definitions.h"
#include "Grid.h"
#include "CompressedIndices.h"

template <class T>
class CompressedValues
{
	private:
		T *_values;
		size_t _count;
		size_t _sizeX, _sizeY, _sizeZ;

	private:
		inline CompressedValues(T *values, size_t count,
								size_t sizeX, size_t sizeY, size_t sizeZ)
			: _values(values), _count(count), _sizeX(sizeX), _sizeY(sizeY), _sizeZ(sizeZ)
		{ /*nothing*/ }

		inline CompressedValues(const CompressedValues &)
		{ throw std::runtime_error("Ata-ta"); }

	public:
		inline T *Pointer()
		{ return _values; }

		inline size_t Count()
		{ return _count; }

		inline bool IsCompatibleWith(CompressedIndices *indices)
		{
			return indices->SizeX() == _sizeX &&
				   indices->SizeY() == _sizeY &&
				   indices->SizeZ() == _sizeZ &&
				   indices->Count() == _count;
		}

		static CompressedValues<T> *Create(Grid<T> *grid, CompressedIndices *indices);

		~CompressedValues()
		{
			if (_values != NULL)
				delete[] _values;
			_values = NULL;
		}
};

template <class T>
CompressedValues<T> *CompressedValues<T>::Create(Grid<T> *grid, CompressedIndices *indices)
{
	if (!indices->IsCompatibleWith(grid))
		throw std::runtime_error("Cannot compress values - wrong indices");

	T *values = new T[indices->Count()];
	for (size_t z = 0; z < indices->SizeZ(); z++)
		for (size_t yID = indices->ZOffsets()[z]; yID < indices->ZOffsets()[z + 1]; yID++)
		{
			size_t y = indices->YIndices()[yID];
			for (size_t xID = indices->YOffsets()[yID]; xID < indices->YOffsets()[yID + 1]; xID++)
			{
				size_t x = indices->XIndices()[xID];
				size_t index = grid->ComputeIndex(x, y, z);
				values[xID] = grid->Pointer()[index];
			}
		}

	return new CompressedValues<T>(values, indices->Count(), indices->SizeX(), indices->SizeY(), indices->SizeZ());
}
