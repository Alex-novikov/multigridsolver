
#pragma once

#include <cstdio>
#include <cstdlib>
#include <memory>
#include <string>
#include <vector>
#include <stdexcept>

typedef double real;
typedef __int32 offset;

typedef __int8 int8;
typedef __int16 int16;
typedef __int32 int32;
typedef __int64 int64;

//Defines which device the locked grid will be processed on.
class MemoryType
{
	public:
		enum Type
		{
			Host		= 1,			//CPU memory block
			MappedHost	= 2,			//CPU memory block that can be accessed from GPU
			Device		= 3				//GPU memory block
		};
};

//Defines type of the computing device.
class DeviceType
{
	public:
		enum Type
		{
			CPU = 1,	//Multi-core CPU
			GPU = 2		//CUDA-compatible GPU
		};
};


//Defines how boundary value must be applied.
class BoundaryCondition
{
	public:
		enum Type
		{
			Dirichlet	= 1,		//Dirichlet boundary condition (fixed value)
			Neumann		= 2			//Neumann boundary condition (fixed derivative)
		};
};

//Defines cell role.
class CellType
{
	public:
		enum Type
		{
			Working		= 0,		//Cell with alive neighbours
			Boundary	= 1,		//Cell that is located on the boundary and have at least one dead neighbour
			Dead		= 2			//Cell is located in unreachable place
		};
};
