
#pragma once

#include "Definitions.h"
#include <algorithm>

//Definition of the used class.
class PerfInfo;

//Contains performance information about one atomic algorithm's stage.
class PerfRecord
{
	private:
		std::string _description;
		double _elapsedTime;
		double _perfMetric, _bwMetric;
		std::string _perfUnits, _bwUnits;

	private:
		static void FormatUnits(double &value, std::string &units);

		inline PerfRecord(const std::string &description, int64 flop, int64 bytes, double elapsedTime)
			:_description(description), _elapsedTime(elapsedTime)
		{
			_perfMetric = flop / std::max(elapsedTime, 1e-7);
			_perfUnits = "Flops";
			FormatUnits(_perfMetric, _perfUnits);

			_bwMetric = bytes / std::max(elapsedTime, 1e-7);
			_bwUnits = "B/s";
			FormatUnits(_bwMetric, _bwUnits);
		}

	public:
		inline const std::string &Description() const
		{ return _description; }

		inline double ElapsedTime() const
		{ return _elapsedTime; }

		inline double PerformanceValue() const
		{ return _perfMetric; }

		inline const std::string &PerformanceUnits() const
		{ return _perfUnits; }

		inline double BandwidthValue() const
		{ return _bwMetric; }

		inline const std::string &BandwidthUnits() const
		{ return _perfUnits; }

	friend class PerfInfo;
};

//Simple container with information about achieved performance.
class PerfInfo
{
	private:
		std::vector<PerfRecord> _records;

	public:
		inline void AddRecord(const char *description, int64 flop, int64 bytes, double elapsedTime)
		{ _records.push_back(PerfRecord(description, flop, bytes, elapsedTime)); }

		inline const std::vector<PerfRecord> &Records() const
		{ return _records; }
};
