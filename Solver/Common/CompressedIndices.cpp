
#include "CompressedIndices.h"

//-------------------------
//--- CompressedIndices ---
//-------------------------

CompressedIndices::~CompressedIndices()
{
	if (_xIndices != NULL)
		delete[] _xIndices;
	_xIndices = NULL;

	if (_yIndices != NULL)
		delete[] _yIndices;
	_yIndices = NULL;

	if (_yOffsets != NULL)
		delete[] _yOffsets;
	_yOffsets = NULL;

	if (_zOffsets != NULL)
		delete[] _zOffsets;
	_zOffsets = NULL;
}
