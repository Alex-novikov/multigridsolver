
#include "MemoryFactory.h"

//-----------------------
//--- HostMemoryBlock ---
//-----------------------

class HostMemoryBlock : public IMemoryBlock
{
	private:
		static inline void *Alloc(size_t sizeInBytes)
		{ return malloc(sizeInBytes); }

	public:
		HostMemoryBlock(size_t sizeInBytes)
			: IMemoryBlock(Alloc(sizeInBytes), MemoryType::Host, sizeInBytes)
		{ /*nothing*/ }

		virtual void Clear()
		{ memset(Pointer(), 0, Size()); }

		virtual ~HostMemoryBlock()
		{
			if (Pointer() != NULL)
				free(Pointer());
		}
};

//---------------------
//--- MemoryFactory ---
//---------------------

IMemoryBlock *MemoryFactory::Allocate(MemoryType::Type memType, size_t sizeInBytes)
{
	if (memType == MemoryType::Host)
	{ return new HostMemoryBlock(sizeInBytes); }
	else
		throw std::runtime_error("NIY");
}

static void Copy(IMemoryBlock *dst, size_t dstOffsetInBytes,
						 IMemoryBlock *src, size_t srcOffsetInBytes, size_t sizeInBytes)
{
	if (dst->Size() < dstOffsetInBytes + sizeInBytes ||
		src->Size() < srcOffsetInBytes + sizeInBytes)
		throw std::runtime_error("Wrong setup for memory block copying");

	if (dst->MemoryType() != MemoryType::Host ||
		src->MemoryType() != MemoryType::Host)
	{
		memcpy((unsigned char *)dst->Pointer() + dstOffsetInBytes,
			   (unsigned char *)src->Pointer() + srcOffsetInBytes,
			   sizeInBytes);
	}
	else
		throw std::runtime_error("NIY");
}
