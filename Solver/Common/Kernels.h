
#pragma once

#include "Definitions.h"
#include "Grid.h"
#include "CompressedIndices.h"
#include "CompressedValues.h"

class IPoissonKernel
{
	public:
		virtual void ApplyBoundary(Grid<real> *layer, Grid<int> *mask,
								   CompressedIndices *boundary,
								   CompressedValues<int> *boundaryType,
								   CompressedValues<real> *dirichletValues,
								   CompressedValues<real> *neumannValues,
								   CompressedValues<real> *nxValues,
								   CompressedValues<real> *nyValues,
								   CompressedValues<real> *nzValues,
								   real dh,
								   Region region) = 0;

		virtual void DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
			Grid<real> **residual, Grid<real> **temp,
			Grid<real> *f,
			CompressedIndices** bIndices,
			CompressedValues<int> ** bType,
			CompressedValues <real> ** nValues,
			CompressedValues <real> ** nxValues,
			CompressedValues <real> ** nyValues,
			CompressedValues <real> ** nzValues,
			real dh, int32 alt,
			Grid<int> **mask, Region** region) = 0;

		virtual void CalculateResidual(Grid<real> *residual, Grid<real> *srcLayer,
			Grid<real> *f, real dh,
			Grid<int> *mask, Region region) = 0;
};


