
#pragma once

#include "Definitions.h"

#include "MemoryFactory.h"

//Represents 3-dimensional region that bounds sub-grid.
class Region
{
	private:
		size_t _offsetX, _offsetY, _offsetZ;
		size_t _sizeX, _sizeY, _sizeZ;

	public:
		inline Region(size_t offsetX, size_t offsetY, size_t offsetZ,
					  size_t sizeX, size_t sizeY, size_t sizeZ)
			: _offsetX(offsetX), _offsetY(offsetY), _offsetZ(offsetZ),
			  _sizeX(sizeX), _sizeY(sizeY), _sizeZ(sizeZ)
		{ /*nothing*/ }

		inline size_t OffsetX()
		{ return _offsetX; }

		inline size_t SizeX()
		{ return _sizeX; }

		inline size_t OffsetY()
		{ return _offsetY; }

		inline size_t SizeY()
		{ return _sizeY; }

		inline size_t OffsetZ()
		{ return _offsetZ; }

		inline size_t SizeZ()
		{ return _sizeZ; }
};

//Represents grid as 3D array with meta information and access helper functions.
template<class T>
class Grid
{
	private:
		IMemoryBlock *_block;
		size_t _sizeX, _sizeY, _sizeZ;
		size_t _pitchYinElements, _pitchZinElements;

	private:
		inline Grid(IMemoryBlock *block,
					size_t sizeX, size_t sizeY, size_t sizeZ,
					size_t pitchYinElements, size_t pitchZinElements)
			 : _block(block), _sizeX(sizeX), _sizeY(sizeY), _sizeZ(sizeZ),
			   _pitchYinElements(pitchYinElements), _pitchZinElements(pitchZinElements)
		{ /*nothing*/ }

		inline Grid(const Grid<T> &)
		{ throw std::runtime_error("Ata-ta"); }

	public:
		inline size_t SizeX() const
		{ return _sizeX; }

		inline size_t SizeY() const
		{ return _sizeY; }

		inline size_t SizeZ() const
		{ return _sizeZ; }

		inline size_t PitchY() const
		{ return _pitchYinElements; }

		inline size_t PitchZ() const
		{ return _pitchZinElements; }

		inline MemoryType::Type MemoryType() const
		{ return _block->MemoryType(); }

		inline T *Pointer() const
		{ return (T *)_block->Pointer(); }

		inline size_t ComputeIndex(size_t x, size_t y, size_t z) const
		{ return x + y * _pitchYinElements + z * _pitchZinElements; }

		inline void Clear()
		{ _block->Clear(); }

		static inline Grid *Create(MemoryType::Type memType, size_t sizeX, size_t sizeY, size_t sizeZ)
		{
			return new Grid<T>(MemoryFactory::Allocate(memType, sizeX * sizeY * sizeZ * sizeof(T)),
							   sizeX, sizeY, sizeZ, sizeX, sizeX * sizeY);
		}

		static void Copy(Grid<T> *dst, size_t dstOffsetX, size_t dstOffsetY, size_t dstOffsetZ,
						 Grid<T> *src, Region srcRegion)
		{
			real *dstP = dst->Pointer();
			real *srcP = src->Pointer();
			size_t pitchY = dst->PitchY();
			size_t pitchZ = dst->PitchZ();

			if (dst->SizeX() < dstOffsetX + srcRegion.SizeX() ||
				dst->SizeY() < dstOffsetY + srcRegion.SizeY() ||
				dst->SizeZ() < dstOffsetZ + srcRegion.SizeZ() ||
				src->SizeX() < srcRegion.OffsetX() + srcRegion.SizeX() ||
				src->SizeY() < srcRegion.OffsetY() + srcRegion.SizeY() ||
				src->SizeZ() < srcRegion.OffsetZ() + srcRegion.SizeZ())
				throw std::runtime_error("Wrong setup for grid copying");

			if (dst->MemoryType() == MemoryType::Device ||
				src->MemoryType() == MemoryType::Device)
				throw std::runtime_error("NIY");
			else
				for (size_t z = srcRegion.OffsetZ(); z < srcRegion.OffsetZ() + srcRegion.SizeZ(); z++)
					for (size_t y = srcRegion.OffsetY(); y < srcRegion.OffsetY() + srcRegion.SizeY(); y++)
						for (size_t x = srcRegion.OffsetX(); x < srcRegion.OffsetX() + srcRegion.SizeX(); x++)
						{
							size_t index = dst->ComputeIndex(x, y, z);
							dstP[index] = srcP[index];
						}
		}

		template <class T1, class T2>
		static inline bool HaveSameIndices(Grid<T1> *grid1, Grid<T2> *grid2)
		{
			return grid1->SizeX() == grid2->SizeX() && grid1->SizeY() == grid2->SizeY() && grid1->SizeZ() == grid2->SizeZ() &&
				   grid1->PitchY() == grid2->PitchY() && grid1->PitchZ() == grid2->PitchZ();
		}

		inline ~Grid()
		{
			if (_block != NULL)
				delete _block;
			_block = NULL;
		}
};
