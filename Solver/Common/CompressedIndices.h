
#pragma once

#include "Definitions.h"
#include "Grid.h"

class CompressedIndices
{
	private:
		size_t _count;
		size_t _sizeX, _sizeY, _sizeZ;
		int *_xIndices;			//contains x-coordinates of non-zero elements, size is equal to _nonZero value
		int *_yIndices;			//contains y-coordinates of non-zero elements
		offset *_yOffsets;		//contains offsets from which y-th row begins
		offset *_zOffsets;		//contains offsets from which z-th layer begins

	private:
		inline CompressedIndices(size_t count,
								 size_t sizeX, size_t sizeY, size_t sizeZ,
								 int *xIndices, int *yIndices, offset *yOffsets, offset *zOffsets)
			: _count(count), _sizeX(sizeX), _sizeY(sizeY), _sizeZ(sizeZ),
			  _xIndices(xIndices), _yIndices(yIndices), _yOffsets(yOffsets), _zOffsets(zOffsets)
		{ /*nothing*/ }

		inline CompressedIndices(const CompressedIndices &)
		{ throw std::runtime_error("Ata-ta"); }

	public:
		inline size_t Count()
		{ return _count; }

		inline size_t SizeX()
		{ return _sizeX; }

		inline size_t SizeY()
		{ return _sizeY; }

		inline size_t SizeZ()
		{ return _sizeZ; }

		inline int *XIndices()
		{ return _xIndices; }

		inline int *YIndices()
		{ return _yIndices; }

		inline offset *YOffsets()
		{ return _yOffsets; }

		inline offset *ZOffsets()
		{ return _zOffsets; }

		template <class T>
		bool IsCompatibleWith(Grid<T> *grid);

		template <class T>
		static CompressedIndices *Create(Grid<T> *grid, bool (*selector)(T value));

		~CompressedIndices();
};

template <class T>
CompressedIndices *CompressedIndices::Create(Grid<T> *grid, bool (*selector)(T value))
{
	T *ptr = grid->Pointer()/*[index]*/;
	size_t valCount = 0;
	size_t yIdCount = 0;

	//Phase 1. Counting selected cells and deteccting array sizes.
	for (size_t z = 0; z < grid->SizeZ(); z++)
		for (size_t y = 0; y < grid->SizeZ(); y++)
		{
			size_t yTmp = valCount;
			for (size_t x = 0; x < grid->SizeZ(); x++)
			{
				int index = grid->ComputeIndex(x, y, z);
				if (selector(ptr[index]))
				{
					valCount += 1;
				}
			}
			if (yTmp != valCount)
				yIdCount += 1;
		}

		//Phase 2. Allocating and initializing arrays.
		size_t curXId = 0;
		size_t curYId = 0;
		int *xIndices = new int[valCount];
		int *yIndices = new int[yIdCount];
		offset *yOffsets = new offset[yIdCount + 1];
		offset *zOffsets = new offset[grid->SizeZ() + 1];

		for (size_t z = 0; z < grid->SizeZ(); z++)
		{
			zOffsets[z] = curYId;

			for (size_t y = 0; y < grid->SizeY(); y++)
			{
				size_t yTmp = curXId;
				
				for (size_t x = 0; x < grid->SizeX(); x++)
				{
					int index = grid->ComputeIndex(x, y, z);
					if (selector(ptr[index]))
					{
						xIndices[curXId] = x;
						curXId += 1;
					}
				}

				if (curXId != yTmp)
				{
					yOffsets[curYId] = yTmp;
					yIndices[curYId] = y;
					curYId += 1;
				}
			}
		}
		yOffsets[yIdCount] = valCount;
		zOffsets[grid->SizeZ()] = yIdCount;

		//Returning indices.
		return new CompressedIndices(valCount, grid->SizeX(), grid->SizeY(), grid->SizeZ(),
									 xIndices, yIndices, yOffsets, zOffsets);
}

template <class T>
bool CompressedIndices::IsCompatibleWith(Grid<T> *grid)
{
	return grid->SizeX() == SizeX() &&
		   grid->SizeY() == SizeY() &&
		   grid->SizeZ() == SizeZ();
}
