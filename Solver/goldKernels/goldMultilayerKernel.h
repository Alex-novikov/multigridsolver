#pragma once


#include  "Common/Kernels.h"
#include "../Helper.h"

class goldMultilayerKernel :
	public IPoissonKernel
{
public:
	void RealGridSave(Grid<real>* grid, const char* filename = "GridSave.csv"){
		Helper::GridSave<real>(grid, filename);
	}
	virtual void ApplyBoundary(Grid<real> *layer, Grid<int> *mask,
		CompressedIndices *boundary,
		CompressedValues<int> *boundaryType,
		CompressedValues<real> *dirichletValues,
		CompressedValues<real> *neumannValues,
		CompressedValues<real> *nxValues,
		CompressedValues<real> *nyValues,
		CompressedValues<real> *nzValues,
		real dh,
		Region region);

	virtual void ApplyNeumannBoundary(Grid<real> *layer, Grid<int> *mask,
		CompressedIndices *boundary,
		CompressedValues<int> *boundaryType,
		CompressedValues<real> *neumannValues,
		CompressedValues<real> *nxValues,
		CompressedValues<real> *nyValues,
		CompressedValues<real> *nzValues,
		real dh,
		Region region);

	virtual void ApplyZeroNeumannBoundary(Grid<real> *layer, Grid<int> *mask,
		CompressedIndices *boundary,
		CompressedValues<int> *boundaryType,
		CompressedValues<real> *nxValues,
		CompressedValues<real> *nyValues,
		CompressedValues<real> *nzValues,
		real dh,
		Region region);

	virtual void DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
		Grid<real> **residual, Grid<real> **temp,
		Grid<real> *f,
		CompressedIndices** bIndices,
		CompressedValues<int> ** bType,
		CompressedValues < real> ** nValues,
		CompressedValues < real> ** nxValues,
		CompressedValues < real> ** nyValues,
		CompressedValues < real> ** nzValues,
		real dh, int32 alt,
		Grid<int> **mask, Region** region);

	virtual void CalculateResidual(Grid<real> *residual, Grid<real> *srcLayer, 
								   Grid<real> *f, real dh, 
								   Grid<int> *mask, Region region);

	inline void NextGridInterpolation(Grid<real>* dstLayer, Grid<real>* srcLayer,
		Grid<int> *dstMask, Grid<int> *srcMask,
		Region dstRegion);

private:
	inline void NextGridSum(Grid<real>* dstLayer, Grid<real>* srcLayer,
						    Grid<int> *dstMask, Grid<int> *srcMask,
							Region dstRegion);

	inline void JacobyMethod(Grid<real> *dstLayer, Grid<real> *srcLayer,
		Grid<real> *f, real dh,
		Grid<int> *mask, Region region);

	inline void ResidualRestriction(Grid<real> *residual,
		Grid<int> *dstMask, Grid<int> *srcMask,
		Region region);

	inline void ResetBoundary(Grid<real> *layer, Grid<int> *mask,
		CompressedIndices *boundary,
		CompressedValues<int> *boundaryType,
		CompressedValues<real> *dirichletValues,
		CompressedValues<real> *neumannValues,
		Region region);
};

