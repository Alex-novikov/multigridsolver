#include "goldMultilayerKernel.h"
//it must be more than zero
#define SMOOTH_ITERATIONS 30

void goldMultilayerKernel::ApplyBoundary(Grid<real> *layer, Grid<int> *mask,
	CompressedIndices *boundary,
	CompressedValues<int> *boundaryType,
	CompressedValues<real> *dirichletValues,
	CompressedValues<real> *neumannValues,
	CompressedValues<real> *nxValues,
	CompressedValues<real> *nyValues,
	CompressedValues<real> *nzValues,
	real dh,
	Region region)
{
	if (!Grid<real>::HaveSameIndices(layer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!dirichletValues->IsCompatibleWith(boundary) ||
		!neumannValues->IsCompatibleWith(boundary))
	{
		throw std::runtime_error("Boundary values/indices are broken");
	}

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
	{
		for (size_t yID = boundary->ZOffsets()[z]; yID < (size_t)boundary->ZOffsets()[z + 1]; yID++)
		{
			int y = boundary->YIndices()[yID];
			for (size_t xID = boundary->YOffsets()[yID]; xID < (size_t)boundary->YOffsets()[yID + 1]; xID++)
			{
				int x = boundary->XIndices()[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)boundaryType->Pointer()[xID];

				if (type == BoundaryCondition::Dirichlet)
					layer->Pointer()[layer->ComputeIndex(x, y, z)] = dirichletValues->Pointer()[xID];
			}
		}

	}

	ApplyNeumannBoundary(layer, mask, boundary, boundaryType, 
		neumannValues, nxValues, nyValues, nzValues, dh, region);
}

void goldMultilayerKernel::ApplyNeumannBoundary(Grid<real> *layer, Grid<int> *mask,
	CompressedIndices *boundary,
	CompressedValues<int> *boundaryType,
	CompressedValues<real> *neumannValues,
	CompressedValues<real> *nxValues,
	CompressedValues<real> *nyValues,
	CompressedValues<real> *nzValues,
	real dh,
	Region region)
{
	if (!Grid<real>::HaveSameIndices(layer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!neumannValues->IsCompatibleWith(boundary))
	{
		throw std::runtime_error("Boundary values/indices are broken");
	}

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
	{
		for (size_t yID = boundary->ZOffsets()[z]; yID < (size_t)boundary->ZOffsets()[z + 1]; yID++)
		{
			int y = boundary->YIndices()[yID];
			for (size_t xID = boundary->YOffsets()[yID]; xID < (size_t)boundary->YOffsets()[yID + 1]; xID++)
			{
				int x = boundary->XIndices()[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)boundaryType->Pointer()[xID];

				//������ �������!!!
				if (type == BoundaryCondition::Neumann){
					size_t index = layer->ComputeIndex(x, y, z);

					size_t pitchY = layer->PitchY();
					size_t pitchZ = layer->PitchZ();

					real sum = - dh * neumannValues->Pointer()[xID];
					/*
					if (nxValues->Pointer()[xID] > 0)
						sum -= nxValues->Pointer()[xID] * (4 * layer->Pointer()[index + 1] - layer->Pointer()[index + 2]);
					else if (nxValues->Pointer()[xID] < 0)
						sum += nxValues->Pointer()[xID] * (4 * layer->Pointer()[index - 1] - layer->Pointer()[index - 2]);

					if (nyValues->Pointer()[xID] > 0)
						sum -= nyValues->Pointer()[xID] * (4 * layer->Pointer()[index + pitchY] - layer->Pointer()[index + 2 * pitchY]);
					else if (nyValues->Pointer()[xID] < 0)
						sum += nyValues->Pointer()[xID] * (4 * layer->Pointer()[index - pitchY] - layer->Pointer()[index - 2 * pitchY]);

					if (nzValues->Pointer()[xID] > 0)
						sum -= nzValues->Pointer()[xID] * (4 * layer->Pointer()[index + pitchZ] - layer->Pointer()[index + 2 * pitchZ]);
					else if (nzValues->Pointer()[xID] < 0)
						sum += nzValues->Pointer()[xID] * (4 * layer->Pointer()[index - pitchZ] - layer->Pointer()[index - 2 * pitchZ]);

					layer->Pointer()[index] = -sum / 3 / (abs(nxValues->Pointer()[xID]) + abs(nyValues->Pointer()[xID]) + abs(nzValues->Pointer()[xID]));
					sum = sum;*/
					
					if (nxValues->Pointer()[xID] > 0)
						sum += nxValues->Pointer()[xID] * layer->Pointer()[index + 1];
					else if (nxValues->Pointer()[xID] < 0)
						sum -= nxValues->Pointer()[xID] * layer->Pointer()[index - 1];

					if (nyValues->Pointer()[xID] > 0)
						sum += nyValues->Pointer()[xID] * layer->Pointer()[index + pitchY];
					else if (nyValues->Pointer()[xID] < 0)
						sum -= nyValues->Pointer()[xID] * layer->Pointer()[index - pitchY];

					if (nzValues->Pointer()[xID] > 0)
						sum += nzValues->Pointer()[xID] * layer->Pointer()[index + pitchZ];
					else if (nzValues->Pointer()[xID] < 0)
						sum -= nzValues->Pointer()[xID] * layer->Pointer()[index - pitchZ];

					layer->Pointer()[index] = sum / (abs(nxValues->Pointer()[xID]) + abs(nyValues->Pointer()[xID]) + abs(nzValues->Pointer()[xID]));
				}
			}
		}

	}

	//Helper::GridPrint<real>(layer);
}

void goldMultilayerKernel::ApplyZeroNeumannBoundary(Grid<real> *layer, Grid<int> *mask,
	CompressedIndices *boundary,
	CompressedValues<int> *boundaryType,
	CompressedValues<real> *nxValues,
	CompressedValues<real> *nyValues,
	CompressedValues<real> *nzValues,
	real dh,
	Region region)
{
	if (!Grid<real>::HaveSameIndices(layer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary))
	{
		throw std::runtime_error("Boundary values/indices are broken");
	}

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
	{
		for (size_t yID = boundary->ZOffsets()[z]; yID < (size_t)boundary->ZOffsets()[z + 1]; yID++)
		{
			int y = boundary->YIndices()[yID];
			for (size_t xID = boundary->YOffsets()[yID]; xID < (size_t)boundary->YOffsets()[yID + 1]; xID++)
			{
				int x = boundary->XIndices()[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)boundaryType->Pointer()[xID];

				//������ �������!!!
				if (type == BoundaryCondition::Neumann){
					size_t index = layer->ComputeIndex(x, y, z);

					size_t pitchY = layer->PitchY();
					size_t pitchZ = layer->PitchZ();

					real sum = 0;
					
					if (nxValues->Pointer()[xID] > 0)
						sum += nxValues->Pointer()[xID] * layer->Pointer()[index + 1];
					else if (nxValues->Pointer()[xID] < 0)
						sum -= nxValues->Pointer()[xID] * layer->Pointer()[index - 1];

					if (nyValues->Pointer()[xID] > 0)
						sum += nyValues->Pointer()[xID] * layer->Pointer()[index + pitchY];
					else if (nyValues->Pointer()[xID] < 0)
						sum -= nyValues->Pointer()[xID] * layer->Pointer()[index - pitchY];

					if (nzValues->Pointer()[xID] > 0)
						sum += nzValues->Pointer()[xID] * layer->Pointer()[index + pitchZ];
					else if (nzValues->Pointer()[xID] < 0)
						sum -= nzValues->Pointer()[xID] * layer->Pointer()[index - pitchZ];

					layer->Pointer()[index] = sum / (abs(nxValues->Pointer()[xID]) + abs(nyValues->Pointer()[xID]) + abs(nzValues->Pointer()[xID]));
					if (isnan(layer->Pointer()[index]))
						throw exception();
				}
			}
		}

	}
}


//���, �������, ����� ���� ������ ������� �������
void goldMultilayerKernel::ResetBoundary(Grid<real> *layer, Grid<int> *mask,
	CompressedIndices *boundary,
	CompressedValues<int> *boundaryType,
	CompressedValues<real> *dirichletValues,
	CompressedValues<real> *neumannValues,
	Region region)
{
	if (!Grid<real>::HaveSameIndices(layer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!dirichletValues->IsCompatibleWith(boundary) ||
		!neumannValues->IsCompatibleWith(boundary))
	{
		throw std::runtime_error("Boundary values/indices are broken");
	}

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
	{
		for (size_t yID = boundary->ZOffsets()[z]; yID < (size_t)boundary->ZOffsets()[z + 1]; yID++)
		{
			int y = boundary->YIndices()[yID];
			for (size_t xID = boundary->YOffsets()[yID]; xID < (size_t)boundary->YOffsets()[yID + 1]; xID++)
			{
				int x = boundary->XIndices()[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)boundaryType->Pointer()[xID];

				if (type == BoundaryCondition::Dirichlet)
					layer->Pointer()[layer->ComputeIndex(x, y, z)] = 0;
				else
					throw std::runtime_error("NIY");
			}
		}

	}
}

//�������� � ������������ ����������
inline void goldMultilayerKernel::NextGridSum(Grid<real>* dstLayer, Grid<real>* srcLayer,
	Grid<int> *dstMask, Grid<int> *srcMask,
	Region dstRegion)
{
	real *dst = dstLayer->Pointer();
	real *src = srcLayer->Pointer();
	int *dMsk = dstMask->Pointer();
	size_t pitchY = dstMask->PitchY();
	size_t pitchZ = dstMask->PitchZ();

	for (size_t z = dstRegion.OffsetZ()+1; z < dstRegion.OffsetZ() + dstRegion.SizeZ()-1; z++)
		for (size_t y = dstRegion.OffsetY()+1; y < dstRegion.OffsetY() + dstRegion.SizeY()-1; y++)
			for (size_t x = dstRegion.OffsetX()+1; x < dstRegion.OffsetX() + dstRegion.SizeX()-1; x++)
			{
				size_t srcIndex = srcMask->ComputeIndex((x-1) / 2 + 1, (y-1) / 2 + 1, (z-1) / 2 + 1);
				size_t dstIndex = dstMask->ComputeIndex(x, y, z);
				// ���������������, ��� ��� ���������� ����������� dead � working ��������� � working
				if ((CellType::Type)dMsk[dstIndex] == CellType::Working)
				{
					dst[dstIndex] += src[srcIndex];
				}
			}
}

inline void goldMultilayerKernel::JacobyMethod(Grid<real> *dstLayer, Grid<real> *srcLayer,
	Grid<real> *f, real dh,
	Grid<int> *mask, Region region)
{
	real *dst = dstLayer->Pointer();
	real *src = srcLayer->Pointer();
	real *fnc = f->Pointer();
	int *msk = mask->Pointer();
	size_t pitchY = mask->PitchY();
	size_t pitchZ = mask->PitchZ();
	real dhdh = dh * dh;

	//Helper::GridPrint<real>(srcLayer);
	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
		for (size_t y = region.OffsetY(); y < region.OffsetY() + region.SizeY(); y++)
			for (size_t x = region.OffsetX(); x < region.OffsetX() + region.SizeX(); x++)
			{
				size_t index = mask->ComputeIndex(x, y, z);
				size_t findex = f->ComputeIndex(x, y, z);
				if ((CellType::Type)msk[index] == CellType::Working)
				{
					dst[index] = (src[index - 1] + src[index + 1] +
						src[index - pitchY] + src[index + pitchY] +
						src[index - pitchZ] + src[index + pitchZ] +
						fnc[findex] * dhdh) / 6;

					if (isnan(dst[index])) 
						throw exception();
				}
			}

	//Helper::GridPrint<real>(dstLayer);
}

//��� ����� ���������� ������ ��� ������������!!!
//+ �������� � ������!
inline void goldMultilayerKernel::ResidualRestriction(Grid<real> *residual,
	Grid<int> *dstMask, Grid<int> *srcMask,
	Region region)
{
	real *rsdl = residual->Pointer();
	int *msk = dstMask->Pointer();
	size_t pitchY = srcMask->PitchY();
	size_t pitchZ = srcMask->PitchZ();

	for (size_t z = region.OffsetZ()+1; z < region.OffsetZ() + region.SizeZ() - 1; z+=2)
		for (size_t y = region.OffsetY()+1; y < region.OffsetY() + region.SizeY() - 1; y+=2)
			for (size_t x = region.OffsetX()+1; x < region.OffsetX() + region.SizeX() - 1; x+=2)
			{
				//��� �� ����� � �� �� ����� �����, ������� ����� ������ �� ���������
				size_t dIndex = srcMask->ComputeIndex(x / 2 + 1 , y / 2 + 1, z / 2 + 1);
				size_t sIndex = srcMask->ComputeIndex(x, y, z);
				//�� ���������� �� ���� ������� ��� �� ������ ����
				if ((CellType::Type)msk[dstMask->ComputeIndex(x / 2 + 1, y / 2 + 1, z / 2 + 1)] == CellType::Working)
				{
					rsdl[dIndex] = (rsdl[sIndex] + rsdl[sIndex + 1] +
						rsdl[sIndex + pitchY] + rsdl[sIndex + pitchY + 1] +
						rsdl[sIndex + pitchZ] + rsdl[sIndex + pitchZ + 1] +
						rsdl[sIndex + pitchY + pitchZ] + rsdl[sIndex + pitchY + pitchZ + 1]) / 8;
				}
			}
	for (size_t z = 0; z < dstMask->SizeZ(); z++)
		for (size_t y = 0; y < dstMask->SizeY(); y++)
			for (size_t x = 0; x < dstMask->SizeX(); x++)
			{
				size_t dIndex = srcMask->ComputeIndex(x, y, z);
				//�� ���������� �� ���� ������� ��� �� ������ ����
				if ((CellType::Type)msk[dstMask->ComputeIndex(x, y, z)] == CellType::Boundary)
				{
					rsdl[dIndex] = 0;
				}
			}
	//���� ������� ��������� �������
	//������ ���� ���
	//for (int i = 0; i < y / step / 2 + 1; i++){
	//	residual[((int)pow(2, alt) + 2)*(1 + i) + x / step / 2 + 1] = 0;
	//	residual[((int)pow(2, alt) + 2)*(x / step / 2 + 1) + 1 + i] = 0;
	//}
	//������, �����, ������� �����
};

///<summary>V-cycle method</summary>
///<param name="dstLayer">Must have applied boundary</param>
void goldMultilayerKernel::DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
	Grid<real> **residual, Grid<real> **temp,
	Grid<real> *f,
	CompressedIndices** bIndices,
	CompressedValues<int> ** bType,
	CompressedValues < real> ** nValues,
	CompressedValues < real> ** nxValues,
	CompressedValues < real> ** nyValues,
	CompressedValues < real> ** nzValues,
	real dh, int32 alt,
	Grid<int> **mask, Region** region)
{
	if (!Grid<real>::HaveSameIndices(dstLayer, srcLayer) ||
		!Grid<real>::HaveSameIndices(dstLayer, mask[alt]) ||
		!Grid<real>::HaveSameIndices(dstLayer, f))
		throw std::runtime_error("Grids with different indices are not supported");

	real* ptr;
	//pre-smoothing
	ApplyNeumannBoundary(temp[alt], mask[alt],
		bIndices[alt], bType[alt],
		nValues[alt],
		nxValues[alt], nyValues[alt], nzValues[alt], dh, *region[alt]);
	JacobyMethod(temp[alt], srcLayer, f, dh, mask[alt], *region[alt]);
	for (int n = 1; n < SMOOTH_ITERATIONS; n++){
		ApplyNeumannBoundary(temp[alt], mask[alt],
			bIndices[alt], bType[alt],
			nValues[alt],
			nxValues[alt], nyValues[alt], nzValues[alt], dh, *region[alt]);
		JacobyMethod(temp[alt], temp[alt], f, dh, mask[alt], *region[alt]);
	}
	
	//moving down
	real tdh = dh;
	CalculateResidual(residual[alt - 1], temp[alt], f, tdh, mask[alt], *region[alt]);
	ResidualRestriction(residual[alt - 1], mask[alt - 1], mask[alt], *region[alt]);
	temp[alt - 1]->Clear(); 
	for (int n = 1; n < SMOOTH_ITERATIONS; n++){
		ApplyZeroNeumannBoundary(temp[alt-1], mask[alt-1],
			bIndices[alt-1], bType[alt-1],
			nxValues[alt - 1], nyValues[alt - 1], nzValues[alt - 1], 1.0 / temp[alt - 1]->SizeX(), *region[alt - 1]);
		JacobyMethod(temp[alt - 1], temp[alt - 1], residual[alt - 1], 1.0 / temp[alt - 1]->SizeX(), mask[alt - 1], *region[alt - 1]);
	}
	
	int talt = alt;
	while (talt > 4) {
		talt--; tdh *= 2;
		CalculateResidual(residual[talt - 1], temp[talt], residual[talt], 1.0 / temp[talt - 1]->SizeX(), mask[talt], *region[talt]);
		ResidualRestriction(residual[talt - 1], mask[talt - 1], mask[talt], *region[talt]);
		temp[talt - 1]->Clear();
		for (int n = 1; n < SMOOTH_ITERATIONS; n++){
			ApplyZeroNeumannBoundary(temp[talt - 1], mask[talt - 1],
				bIndices[talt - 1], bType[talt - 1],
				nxValues[talt - 1], nyValues[talt - 1], nzValues[talt - 1], 1.0 / temp[talt - 1]->SizeX(), *region[talt - 1]);
			JacobyMethod(temp[talt - 1], temp[talt - 1], residual[talt - 1], 1.0/temp[talt-1]->SizeX(), mask[talt - 1], *region[talt - 1]);
		}
		//Helper::GridSave <real>(temp[talt - 1], "down.csv");
	}

	//moving up
	while (talt < 7){
		//u+ interpolated v
		NextGridSum(temp[talt], temp[talt - 1], mask[talt], mask[talt - 1], *region[talt]);
		//post-smoothing
		for (int n = 0; n < SMOOTH_ITERATIONS; n++){
			ApplyZeroNeumannBoundary(temp[talt], mask[talt],
				bIndices[talt], bType[talt],
				nxValues[talt], nyValues[talt], nzValues[talt], 1.0 / temp[talt]->SizeX(), *region[talt]);
			JacobyMethod(temp[talt], temp[talt], residual[talt], 1.0 / temp[talt]->SizeX(), mask[talt], *region[talt]);
		}
		talt++; tdh /= 2;
	}
	
	//u + interpolated v
	NextGridSum(temp[alt], temp[alt-1], mask[alt], mask[alt-1], *region[alt]);
	//post-smoothing
	
	ApplyNeumannBoundary(temp[alt], mask[alt],
		bIndices[alt], bType[alt],
		nValues[alt],
		nxValues[alt], nyValues[alt], nzValues[alt], dh, *region[alt]);
	JacobyMethod(dstLayer, temp[alt], f, dh, mask[alt], *region[alt]);
	for (int n = 0; n < SMOOTH_ITERATIONS; n++){
		ApplyNeumannBoundary(dstLayer, mask[alt],
			bIndices[alt], bType[alt],
			nValues[alt],
			nxValues[alt], nyValues[alt], nzValues[alt], dh, *region[alt]);
		JacobyMethod(dstLayer, dstLayer, f, dh, mask[alt], *region[alt]);
	}
}

void goldMultilayerKernel::CalculateResidual(Grid<real> *residual, Grid<real> *srcLayer,
	Grid<real> *f, real dh,
	Grid<int> *mask, Region region)
{
	real *dst = residual->Pointer();
	real *src = srcLayer->Pointer();
	real *fnc = f->Pointer();
	int *msk = mask->Pointer();
	size_t pitchY = mask->PitchY();
	size_t pitchZ = mask->PitchZ();
	real dhdh = dh * dh;

	for (size_t z = region.OffsetZ()+1; z < region.OffsetZ() + region.SizeZ()-1; z++)
		for (size_t y = region.OffsetY()+1; y < region.OffsetY() + region.SizeY()-1; y++)
			for (size_t x = region.OffsetX()+1; x < region.OffsetX() + region.SizeX()-1; x++)
			{
				size_t index = mask->ComputeIndex(x, y, z);
				size_t findex = f->ComputeIndex(x, y, z);
				if ((CellType::Type)msk[index] == CellType::Working)
				{
					dst[index] = (src[index - 1] + src[index + 1] +
						src[index - pitchY] + src[index + pitchY] +
						src[index - pitchZ] + src[index + pitchZ] -
						6 * src[index]) / dhdh + fnc[findex];
				}
			}
}
