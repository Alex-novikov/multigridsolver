
#pragma once

#include "Common/Kernels.h"

class goldPoissonKernel : public IPoissonKernel
{
	public:
		virtual void ApplyBoundary(Grid<real> *layer, Grid<int> *mask,
								   CompressedIndices *boundary,
								   CompressedValues<int> *boundaryType,
								   CompressedValues<real> *dirichletValues,
								   CompressedValues<real> *neumannValues,
								   Region region);

		virtual void DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
								 Grid<real> *f, real dh,
								 Grid<int> *mask, Region region);

		virtual real CalculateResidual(Grid<real> *dstLayer, Grid<real> *srcLayer, Grid<int> *mask, Region region);
};
