#pragma once

#include "Common\Grid.h"
#include "Common\Definitions.h"
#include "Common\CompressedIndices.h"
#include "Common\CompressedValues.h"
#include "assert.h"
#include <iostream>
#include <fstream>

using namespace std;

class Helper
{
public:

	static bool neumannSelector(int value){
		if (value == (int)BoundaryCondition::Neumann)
			return true;
		return false;
	}

	template< class T >
	static void SelectiveRestriction(Grid<T>* dstLayer, Grid<T>* srcLayer, Region dstRegion, Region srcRegion){
		T *dst = dstLayer->Pointer();
		T *src = srcLayer->Pointer();
		size_t ratioX = (srcLayer->SizeX() - 2) / (dstLayer->SizeX() - 2);
		size_t ratioY = (srcLayer->SizeY() - 2) / (dstLayer->SizeY() - 2);
		size_t ratioZ = (srcLayer->SizeZ() - 2) / (dstLayer->SizeZ() - 2);

		if (!(ratioX && ratioY && ratioZ))
			throw std::runtime_error("The destination grid is finer then the source grid while restriction.");


		//� ��������� ��� ����� ���, ����� �� ����� ���� ������ �������������� if �� �������
		//�������� ����������� CUDA ��� ��� ����� � ��� ������ �� �����
		size_t srcIndex, dstIndex;
		for (size_t z = dstRegion.OffsetZ(); z < dstRegion.OffsetZ() + dstRegion.SizeZ() - 1; z++){
			for (size_t y = dstRegion.OffsetY(); y < dstRegion.OffsetY() + dstRegion.SizeY() - 1; y++){
				for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
				{
					srcIndex = srcLayer->ComputeIndex(x * ratioX, y * ratioY, z * ratioZ);
					dstIndex = dstLayer->ComputeIndex(x, y, z);
					assert(dstIndex < dstLayer->SizeX()*dstLayer->SizeY()*dstLayer->SizeZ());
					assert(srcIndex < srcLayer->SizeX()*srcLayer->SizeY()*srcLayer->SizeZ());
					dst[dstIndex] = src[srcIndex];
				}
				//on X bound {
				srcIndex = srcLayer->ComputeIndex(srcRegion.OffsetX() + srcRegion.SizeX() - 1, y * ratioY, z * ratioZ);
				dstIndex = dstLayer->ComputeIndex(dstRegion.OffsetX() + dstRegion.SizeX() - 1, y, z);
				assert(dstIndex < dstLayer->SizeX()*dstLayer->SizeY()*dstLayer->SizeZ());
				assert(srcIndex < srcLayer->SizeX()*srcLayer->SizeY()*srcLayer->SizeZ());
				dst[dstIndex] = src[srcIndex];
				//}
			}
			//on Y bound {
			for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
			{
				srcIndex = srcLayer->ComputeIndex(x * ratioX, srcRegion.OffsetY() + srcRegion.SizeY() - 1, z * ratioZ);
				dstIndex = dstLayer->ComputeIndex(x, dstRegion.OffsetY() + dstRegion.SizeY() - 1, z);
				assert(dstIndex < dstLayer->SizeX()*dstLayer->SizeY()*dstLayer->SizeZ());
				assert(srcIndex < srcLayer->SizeX()*srcLayer->SizeY()*srcLayer->SizeZ());
				dst[dstIndex] = src[srcIndex];
			}
			srcIndex = srcLayer->ComputeIndex(srcRegion.OffsetX() + srcRegion.SizeX() - 1, srcRegion.OffsetY() + srcRegion.SizeY() - 1, z * ratioZ);
			dstIndex = dstLayer->ComputeIndex(dstRegion.OffsetX() + dstRegion.SizeX() - 1, dstRegion.OffsetY() + dstRegion.SizeY() - 1, z);
			assert(dstIndex < dstLayer->SizeX()*dstLayer->SizeY()*dstLayer->SizeZ());
			assert(srcIndex < srcLayer->SizeX()*srcLayer->SizeY()*srcLayer->SizeZ());
			dst[dstIndex] = src[srcIndex];
			//}
		}

		//on Z bound {
		for (size_t y = dstRegion.OffsetY(); y < dstRegion.OffsetY() + dstRegion.SizeY() - 1; y++){
			for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
			{
				srcIndex = srcLayer->ComputeIndex(x * ratioX, y * ratioY, srcRegion.OffsetZ() + srcRegion.SizeZ() - 1);
				dstIndex = dstLayer->ComputeIndex(x, y, dstRegion.OffsetZ() + dstRegion.SizeZ() - 1);
				assert(dstIndex < dstLayer->SizeX()*dstLayer->SizeY()*dstLayer->SizeZ());
				assert(srcIndex < srcLayer->SizeX()*srcLayer->SizeY()*srcLayer->SizeZ());
				dst[dstIndex] = src[srcIndex];
			}
			//on X bound {
			srcIndex = srcLayer->ComputeIndex(srcRegion.OffsetX() + srcRegion.SizeX() - 1, y * ratioY, srcRegion.OffsetZ() + srcRegion.SizeZ() - 1);
			dstIndex = dstLayer->ComputeIndex(dstRegion.OffsetX() + dstRegion.SizeX() - 1, y, dstRegion.OffsetZ() + dstRegion.SizeZ() - 1);
			assert(dstIndex < dstLayer->SizeX()*dstLayer->SizeY()*dstLayer->SizeZ());
			assert(srcIndex < srcLayer->SizeX()*srcLayer->SizeY()*srcLayer->SizeZ());
			dst[dstIndex] = src[srcIndex];
			//}
		}
		//on Y bound{
		for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
		{
			srcIndex = srcLayer->ComputeIndex(x * ratioX,
				srcRegion.OffsetY() + srcRegion.SizeY() - 1,
				srcRegion.OffsetZ() + srcRegion.SizeZ() - 1);
			dstIndex = dstLayer->ComputeIndex(x,
				dstRegion.OffsetY() + dstRegion.SizeY() - 1,
				dstRegion.OffsetZ() + dstRegion.SizeZ() - 1);
			assert(dstIndex < dstLayer->SizeX()*dstLayer->SizeY()*dstLayer->SizeZ());
			assert(srcIndex < srcLayer->SizeX()*srcLayer->SizeY()*srcLayer->SizeZ());
			dst[dstIndex] = src[srcIndex];
		}
		//}
		srcIndex = srcLayer->ComputeIndex(srcRegion.OffsetX() + srcRegion.SizeX() - 1,
			srcRegion.OffsetY() + srcRegion.SizeY() - 1,
			srcRegion.OffsetZ() + srcRegion.SizeZ() - 1);
		dstIndex = dstLayer->ComputeIndex(dstRegion.OffsetX() + dstRegion.SizeX() - 1,
			dstRegion.OffsetY() + dstRegion.SizeY() - 1,
			dstRegion.OffsetZ() + dstRegion.SizeZ() - 1);
		assert(dstIndex < dstLayer->SizeX()*dstLayer->SizeY()*dstLayer->SizeZ());
		assert(srcIndex < srcLayer->SizeX()*srcLayer->SizeY()*srcLayer->SizeZ());
		dst[dstIndex] = src[srcIndex];
		//}
	}

	
	template< class T >
	static Grid<T>* DecompressGrid(CompressedIndices* srcIndices, CompressedValues <T>* srcCValues){
		Grid<T>* srcValues = Grid<T>::Create(MemoryType::Host, srcIndices->SizeX(), srcIndices->SizeY(), srcIndices->SizeZ());
		srcValues->Clear();
		T* sPtr = srcValues->Pointer();

		for (size_t z = 0; z < srcValues->SizeZ(); z++)
		{
			for (size_t yID = srcIndices->ZOffsets()[z]; yID < (size_t)srcIndices->ZOffsets()[z + 1]; yID++)
			{
				int y = srcIndices->YIndices()[yID];
				for (size_t xID = srcIndices->YOffsets()[yID]; xID < (size_t)srcIndices->YOffsets()[yID + 1]; xID++)
				{
					int x = srcIndices->XIndices()[xID];
					int index = srcValues->ComputeIndex(x, y, z);
					srcValues->Pointer()[index] = srcCValues->Pointer()[xID];
				}
			}
		}
		return srcValues;
	}

	static void SmartRestriction(Grid<int>* srcMask,CompressedIndices* sindices,
		CompressedValues<int>* srcBnd,
		CompressedValues <real>* snValues, CompressedValues <real>* snxValues,
		CompressedValues <real>* snyValues, CompressedValues <real>* snzValues,
		Region srcRegion,
		Grid<int>* dstMask,
		CompressedIndices** dindices,
		CompressedValues<int>**dBndr,
		CompressedValues <real>** dnValues, CompressedValues <real>** dnxValues,
		CompressedValues <real>** dnyValues, CompressedValues <real>** dnzValues,
		Region dstRegion)
	{

		Grid<int>* srcBndr = DecompressGrid<int>(sindices, srcBnd);
		Grid<real>* sn = DecompressGrid<real>(sindices, snValues);
		Grid<real>* snx = DecompressGrid<real>(sindices, snxValues);
		Grid<real>* sny = DecompressGrid<real>(sindices, snyValues);
		Grid<real>* snz = DecompressGrid<real>(sindices, snzValues);

		Grid<real>* dn = Grid<real>::Create(MemoryType::Host, dstMask->SizeX(), dstMask->SizeY(), dstMask->SizeZ());
		dn->Clear();
		Grid<real>* dnx = Grid<real>::Create(MemoryType::Host, dstMask->SizeX(), dstMask->SizeY(), dstMask->SizeZ());
		dnx->Clear();
		Grid<real>* dny = Grid<real>::Create(MemoryType::Host, dstMask->SizeX(), dstMask->SizeY(), dstMask->SizeZ());
		dny->Clear();
		Grid<real>* dnz = Grid<real>::Create(MemoryType::Host, dstMask->SizeX(), dstMask->SizeY(), dstMask->SizeZ());
		dnz->Clear();

		Grid<int>* dstBndr = Grid<int>::Create(MemoryType::Host, dstMask->SizeX(), dstMask->SizeY(), dstMask->SizeZ());
		dstBndr->Clear();
	
		for (size_t z = dstRegion.OffsetZ() + 1; z < dstRegion.OffsetZ() + dstRegion.SizeZ() - 1; z++){
			for (size_t y = dstRegion.OffsetY() + 1; y < dstRegion.OffsetY() + dstRegion.SizeY() - 1; y++){
				for (size_t x = dstRegion.OffsetX() + 1; x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++){
					size_t srcIndex = srcMask->ComputeIndex(x * 2, y * 2, z * 2);
					size_t dstIndex = dstMask->ComputeIndex(x, y, z);
					int ncount = 0;
					real nval = 0, nxval = 0, nyval = 0, nzval = 0;
					if (srcBndr->Pointer()[srcIndex] == BoundaryCondition::Neumann){
						ncount++;
						nval += sn->Pointer()[srcIndex];
						nxval += snx->Pointer()[srcIndex];
						nyval += sny->Pointer()[srcIndex];
						nyval += snz->Pointer()[srcIndex];
					}
					if (srcBndr->Pointer()[srcIndex + 1] == BoundaryCondition::Neumann){
						ncount++;
						nval += sn->Pointer()[srcIndex + 1];
						nxval += snx->Pointer()[srcIndex + 1];
						nyval += sny->Pointer()[srcIndex + 1];
						nyval += snz->Pointer()[srcIndex + 1];
					}
					if (srcBndr->Pointer()[srcIndex + srcBndr->PitchY()] == BoundaryCondition::Neumann){
						ncount++;
						nval += sn->Pointer()[srcIndex + srcBndr->PitchY()];
						nxval += snx->Pointer()[srcIndex + srcBndr->PitchY()];
						nyval += sny->Pointer()[srcIndex + srcBndr->PitchY()];
						nyval += snz->Pointer()[srcIndex + srcBndr->PitchY()];
					}
					if (srcBndr->Pointer()[srcIndex + srcBndr->PitchZ()] == BoundaryCondition::Neumann){
						ncount++;
						nval += sn->Pointer()[srcIndex + srcBndr->PitchZ()];
						nxval += snx->Pointer()[srcIndex + srcBndr->PitchZ()];
						nyval += sny->Pointer()[srcIndex + srcBndr->PitchZ()];
						nyval += snz->Pointer()[srcIndex + srcBndr->PitchZ()];
					}
					if (srcBndr->Pointer()[srcIndex + srcBndr->PitchY() + 1] == BoundaryCondition::Neumann){
						ncount++;
						nval += sn->Pointer()[srcIndex + srcBndr->PitchY() + 1];
						nxval += snx->Pointer()[srcIndex + srcBndr->PitchY() + 1];
						nyval += sny->Pointer()[srcIndex + srcBndr->PitchY() + 1];
						nyval += snz->Pointer()[srcIndex + srcBndr->PitchY() + 1];
					}
					if (srcBndr->Pointer()[srcIndex + srcBndr->PitchZ() + 1] == BoundaryCondition::Neumann){
						ncount++;
						nval += sn->Pointer()[srcIndex + srcBndr->PitchZ() + 1];
						nxval += snx->Pointer()[srcIndex + srcBndr->PitchZ() + 1];
						nyval += sny->Pointer()[srcIndex + srcBndr->PitchZ() + 1];
						nyval += snz->Pointer()[srcIndex + srcBndr->PitchZ() + 1];
					}
					if (srcBndr->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY()] == BoundaryCondition::Neumann){
						ncount++;
						nval += sn->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY()];
						nxval += snx->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY()];
						nyval += sny->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY()];
						nyval += snz->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY()];
					}
					if (srcBndr->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY() + 1] == BoundaryCondition::Neumann){
						ncount++;
						nval += sn->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY() + 1];
						nxval += snx->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY() + 1];
						nyval += sny->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY() + 1];
						nyval += snz->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY() + 1];
					}

					if (ncount != 0){
						dstMask->Pointer()[dstIndex] = CellType::Boundary;
						dstBndr->Pointer()[dstIndex] = BoundaryCondition::Neumann;
						dn->Pointer()[dstIndex] = nval / ncount;
						dnx->Pointer()[dstIndex] = nxval / ncount;
						dny->Pointer()[dstIndex] = nyval / ncount;
						dnz->Pointer()[dstIndex] = nzval / ncount;
					}
					else if (srcMask->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY() + 1] == CellType::Dead
						|| srcMask->Pointer()[srcIndex + srcBndr->PitchZ() + srcBndr->PitchY()] == CellType::Dead
						|| srcMask->Pointer()[srcIndex + srcBndr->PitchZ() + 1] == CellType::Dead
						|| srcMask->Pointer()[srcIndex + srcBndr->PitchY() + 1] == CellType::Dead
						|| srcMask->Pointer()[srcIndex + srcBndr->PitchZ()] == CellType::Dead
						|| srcMask->Pointer()[srcIndex + srcBndr->PitchY()] == CellType::Dead
						|| srcMask->Pointer()[srcIndex + 1] == CellType::Dead
						|| srcMask->Pointer()[srcIndex] == CellType::Dead)
						dstMask->Pointer()[dstIndex] = CellType::Dead;

				}
			}
		}

			*dindices = CompressedIndices::Create(dstBndr, &Helper::neumannSelector);
			*dnValues = CompressedValues<real>::Create(dn, *dindices);
			delete dn;
			*dnxValues = CompressedValues<real>::Create(dnx, *dindices);
			delete dnx;			
			*dnyValues = CompressedValues<real>::Create(dny, *dindices);
			delete dny;
			*dnzValues = CompressedValues<real>::Create(dnz, *dindices);
			delete dnz;
			*dBndr = CompressedValues<int>::Create(dstBndr, *dindices);
			delete dstBndr;
	};
	

	template< class T >
	static void GridPrint(Grid<T>* grid){
		cout << "GridPrint:" << endl;
		for (size_t z = 0; z < grid->SizeZ(); z++){
			for (size_t y = 0; y < grid->SizeY(); y++){
				for (size_t x = 0; x < grid->SizeX(); x++){
					int index = grid->ComputeIndex(x, y, z);
					cout.setf(ios::fixed);
					cout.precision(2);
					cout << (T)grid->Pointer()[index] << " ";
				}
				cout << endl;
			}
			cout << "\n" << endl;
		}
	}

	template< class T >
	static void GridSave(Grid<T>* grid, const char* filename = "GridSave.csv"){
		ofstream f;
		f.open(filename);


		std::cout << "error: " << strerror(errno) << std::endl;

		f /*<< "X" << "," << "Y" << "," << "Z" << "," */<< "Data";

		for (size_t z = 0; z < grid->SizeZ() - 1; z++)
			f << ",";
		f << endl;

		for (size_t y = 0; y < grid->SizeY(); y++){
			/*f << y << "," << y << "," << y;*/
			for (size_t x = 0; x < grid->SizeX(); x++){
				int index = grid->ComputeIndex(x, y, 0);
				if (x == 0)
					f << (T)grid->Pointer()[index];
				else
					f << "," << (T)grid->Pointer()[index];
			}
			f << endl;
		}
		for (size_t z = 1; z < grid->SizeZ(); z++)
			for (size_t y = 0; y < grid->SizeY(); y++){
				//f << ",,";
				for (size_t x = 0; x < grid->SizeX(); x++){
					int index = grid->ComputeIndex(x, y, z);
					if (x==0)
						f << (T)grid->Pointer()[index];
					else
						f << "," << (T)grid->Pointer()[index];
				}
				f << endl;
			}
		f << endl;
		f << "ID,Column,Variable Name,Data Type,Rank,Missing Value,Dimensions" << endl;
		f << "1,A:DZ,Data,Double,3,,_1:" << grid->SizeX() << " _2:" << grid->SizeY() << " _3:" << grid->SizeZ();
		f.close();
	}

	template< class T >
	static void �GridPrint(CompressedIndices* indices, CompressedValues<T>* cvalues){

		cout << "CGridPrint:" << endl;
		Grid<T>* grid = Grid<T>::Create(MemoryType::Host, indices->SizeX(), indices->SizeY(), indices->SizeZ());
		grid->Clear();

		for (size_t z = 0; z < indices->SizeZ(); z++)
		{
			for (size_t yID = indices->ZOffsets()[z]; yID < (size_t)indices->ZOffsets()[z + 1]; yID++)
			{
				int y = indices->YIndices()[yID];
				for (size_t xID = indices->YOffsets()[yID]; xID < (size_t)indices->YOffsets()[yID + 1]; xID++)
				{
					int x = indices->XIndices()[xID];
					int index = grid->ComputeIndex(x, y, z);
					assert(index < grid->SizeX()*grid->SizeY()*grid->SizeZ());
					grid->Pointer()[index] = cvalues->Pointer()[xID];
				}
			}
		}

		GridPrint<T>(grid);

		delete grid;
	}

	template< class T >
	static Grid<T>* NeumannDataRecovery(CompressedIndices* srcIndices, CompressedValues<T>* srcCValues, Grid<int>* srcMask, CompressedValues<int>* srcBMask, Grid<int>* dstMask, Grid<int>* dstBMask){
		int* bMPtr = dstBMask->Pointer();
		int* mPtr = dstMask->Pointer();

		int spitchY = srcMask->PitchY();
		int spitchZ = srcMask->PitchZ();

		int dpitchY = dstBMask->PitchY();
		int dpitchZ = dstBMask->PitchZ();


		Grid<T>* result = Grid<T>::Create(MemoryType::Host, dstMask->SizeX(), dstMask->SizeY(), dstMask->SizeZ());
		T* rPtr = result->Pointer();
		result->Clear();


		Grid<T>* srcValues = Grid<T>::Create(MemoryType::Host, srcMask->SizeX(), srcMask->SizeY(), srcMask->SizeZ());
		srcValues->Clear();
		T* sPtr = srcValues->Pointer();

		Grid<int>* sBMask = Grid<int>::Create(MemoryType::Host, srcMask->SizeX(), srcMask->SizeY(), srcMask->SizeZ());
		sBMask->Clear();
		int* sBPtr = sBMask->Pointer();

		for (size_t z = 0; z < srcValues->SizeZ(); z++)
		{
			for (size_t yID = srcIndices->ZOffsets()[z]; yID < (size_t)srcIndices->ZOffsets()[z + 1]; yID++)
			{
				int y = srcIndices->YIndices()[yID];
				for (size_t xID = srcIndices->YOffsets()[yID]; xID < (size_t)srcIndices->YOffsets()[yID + 1]; xID++)
				{
					int x = srcIndices->XIndices()[xID];
					int index = srcValues->ComputeIndex(x, y, z);
					assert(index < srcValues->SizeX()*srcValues->SizeY()*srcValues->SizeZ());
					assert(index < sBMask->SizeX()*sBMask->SizeY()*sBMask->SizeZ());
					srcValues->Pointer()[index] = srcCValues->Pointer()[xID];
					sBMask->Pointer()[index] = srcBMask->Pointer()[xID];
				}
			}
		}

		size_t srcSize = sBMask->SizeX()*sBMask->SizeY()*sBMask->SizeX();
		for (size_t z = 1; z < dstBMask->SizeZ() - 1; z++)
		{
			for (size_t y = 1; y < dstBMask->SizeY() - 1; y++)
			{
				for (size_t x = 1; x < dstBMask->SizeX() - 1; x++)
				{
					int dIndex = dstBMask->ComputeIndex(x, y, z);
					int sIndex = srcMask->ComputeIndex(2 * x, 2 * y, 2 * z);

					if (dstMask->Pointer()[dIndex] == CellType::Boundary)
					{
						if (srcMask->Pointer()[sIndex] == CellType::Boundary){
							rPtr[dIndex] = sPtr[sIndex];
							if (rPtr[dIndex] == 0)
								throw exception();
						}
						else if (bMPtr[dIndex] == BoundaryCondition::Neumann)
						{
							assert(dIndex + result->PitchZ() < result->SizeX()*result->SizeY()*result->SizeZ());
							assert(sIndex + srcValues->PitchZ() < srcValues->SizeX()*srcValues->SizeY()*srcValues->SizeZ());
							if (sBPtr[sIndex + 1] == BoundaryCondition::Neumann)
								rPtr[dIndex] = sPtr[sIndex + 1];
							else if (sBPtr[sIndex - 1] == BoundaryCondition::Neumann)
								rPtr[dIndex] = sPtr[sIndex - 1];

							if (sIndex + spitchY < srcSize)
								if (sBPtr[sIndex + spitchY] == BoundaryCondition::Neumann)
									rPtr[dIndex] = sPtr[sIndex + spitchY];

							if (sIndex - spitchY > 0)
								if (sBPtr[sIndex - spitchY] == BoundaryCondition::Neumann)
									rPtr[dIndex] = sPtr[sIndex - spitchY];

							if (sIndex + spitchZ < srcSize)
								if (sBPtr[sIndex + spitchZ] == BoundaryCondition::Neumann)
									rPtr[dIndex] = sPtr[sIndex + spitchZ];

							if (sIndex - spitchZ > 0)
								if (sBPtr[sIndex - spitchZ] == BoundaryCondition::Neumann)
									rPtr[dIndex] = sPtr[sIndex - spitchZ];
							if (rPtr[dIndex] == 0)
								throw exception();
						}
					}
				}
			}
		}

		delete srcValues;
		delete sBMask;
		return result;
	};

	static Grid<int>* BoundaryRecovery(Grid<int>* dstMask, CompressedIndices *srcBoundary, CompressedValues<int> *srcBoundaryType){
		/*if (dstMask->SizeX() != 2 * srcMask->SizeX()
		|| dstMask->SizeX() != 2 * srcMask->SizeX()
		|| dstMask->SizeX() != 2 * srcMask->SizeX())
		throw std::runtime_error("The destination mask must be two times less then source");

		if (dstBMask->SizeX() != 2 * srcBMask->SizeX()
		|| dstBMask->SizeX() != 2 * srcBMask->SizeX()
		|| dstBMask->SizeX() != 2 * srcBMask->SizeX())
		throw std::runtime_error("The destination boundary mask must be two times less then source");

		if (dstRegion.SizeX() != 2 * srcRegion.SizeX()
		|| dstRegion.SizeX() != 2 * srcRegion.SizeX()
		|| dstRegion.SizeX() != 2 * srcRegion.SizeX())
		throw std::runtime_error("The destination region must be two times less then source");
		*/

		Grid<int>* temp = Grid<int>::Create(MemoryType::Host, srcBoundary->SizeX(), srcBoundary->SizeY(), srcBoundary->SizeZ());
		temp->Clear();
		Grid<int>* bMask = Grid<int>::Create(MemoryType::Host, dstMask->SizeX(), dstMask->SizeY(), dstMask->SizeZ());
		bMask->Clear();
		int* bMPtr = bMask->Pointer();
		int* mPtr = dstMask->Pointer();

		int pitchY = bMask->PitchY();
		int pitchZ = bMask->PitchZ();

		for (size_t z = 0; z < temp->SizeZ(); z++)
		{
			for (size_t yID = srcBoundary->ZOffsets()[z]; yID < (size_t)srcBoundary->ZOffsets()[z + 1]; yID++)
			{
				int y = srcBoundary->YIndices()[yID];
				for (size_t xID = srcBoundary->YOffsets()[yID]; xID < (size_t)srcBoundary->YOffsets()[yID + 1]; xID++)
				{
					int x = srcBoundary->XIndices()[xID];
					BoundaryCondition::Type type = (BoundaryCondition::Type)srcBoundaryType->Pointer()[xID];

					size_t index = temp->ComputeIndex(x, y, z);

					assert(index < temp->SizeX()*temp->SizeY()*temp->SizeZ());

					if (type == BoundaryCondition::Neumann){
						temp->Pointer()[index] = BoundaryCondition::Neumann;
					}
					else if (type == BoundaryCondition::Dirichlet){
						temp->Pointer()[index] = BoundaryCondition::Dirichlet;
					}
				}
			}
		}

		//	GridPrint<int>(temp);
		SelectiveRestriction(bMask, temp, Region(0, 0, 0, bMask->SizeX(), bMask->SizeY(), bMask->SizeZ()), Region(0, 0, 0, temp->SizeX(), temp->SizeY(), temp->SizeZ()));
		delete temp;
		//GridPrint<int>(bMask);
		for (size_t z = 1; z < bMask->SizeZ() - 1; z++)
			for (size_t y = 1; y < bMask->SizeY() - 1; y++)
				for (size_t x = 1; x < bMask->SizeX() - 1; x++)
				{
					int index = bMask->ComputeIndex(x, y, z);

					int maxIndex = dstMask->SizeX()*dstMask->SizeY()*dstMask->SizeZ();
					assert(index + pitchZ < maxIndex);
					assert(index - pitchZ >= 0);
					if (mPtr[index + 1] == (int)CellType::Dead
						|| mPtr[index - 1] == (int)CellType::Dead
						|| mPtr[index + pitchY] == (int)CellType::Dead
						|| mPtr[index - pitchY] == (int)CellType::Dead
						|| mPtr[index + pitchZ] == (int)CellType::Dead
						|| mPtr[index - pitchZ] == (int)CellType::Dead)
					{
						bMPtr[index] = (int)BoundaryCondition::Neumann;
						mPtr[index] = (int)CellType::Boundary;
					}
				}
		return bMask;
	}

	~Helper();
};

