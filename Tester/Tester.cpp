#include "Solver\Helper.h"
#include "Solver/All.h"
#define M_PI 3.14159265358979323846
bool selector(int value){
	if (value == (int)CellType::Boundary)
		return true;
	return false;
}
int main(int argc, char *argv[])
{/*
	size_t NX = 130, NY = 130, NZ = 130;
	PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);

	std::vector<std::pair<DeviceDescriptor, float> > devices;
	devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
	solver->Configure(devices);

	Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *direchletValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *neumannValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	neumannValues->Clear();
	Grid<real> *neumannXValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *neumannYValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *neumannZValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);

	for (size_t z = 0; z < NZ; z++)
		for (size_t y = 0; y < NY; y++)
			for (size_t x = 0; x < NX; x++)
			{
				bool isDirichlet = (x == 0) || (x == NX - 1) ||
					(y == 0) || (y == NY - 1) ||
					(z == 0) || (z == NZ - 1);

				int index = boundaryMask->ComputeIndex(x, y, z);

				real rx = (real)x / (NX - 1) - 0.5;
				real ry = (real)y / (NY - 1) - 0.5;
				real rz = (real)z / (NZ - 1) - 0.5;

				real r2 = rx*rx + ry*ry + rz*rz;
				real l = sqrt(r2);
				if (isDirichlet){
					boundaryMask->Pointer()[index] = (int)BoundaryCondition::Dirichlet;
					if (ry>0)
						direchletValues->Pointer()[index] = 0.25 / 2 / r2*sin(acos(rz / l))*sin(-atan(ry / rx));
					else
						direchletValues->Pointer()[index] = 0.25 / 2 / r2*sin(acos(rz / l))*sin(-M_PI-atan(ry / rx));
					mask->Pointer()[index] = (int)CellType::Boundary;
				}
				else {
					//r=0.25
					if ((rx*rx) + (ry*ry) + (rz*rz) < 0.0625)
					{

						mask->Pointer()[index] = (int)CellType::Dead;
					}
					else {
						mask->Pointer()[index] = (int)CellType::Working;
					}

					boundaryMask->Pointer()[index] = 0;
					direchletValues->Pointer()[index] = 0.0f;
				}
			}

	int* bMPtr = boundaryMask->Pointer();
	int* mPtr = mask->Pointer();

	for (size_t z = 0; z < NZ; z++)
		for (size_t y = 0; y < NY; y++)
			for (size_t x = 0; x < NX; x++)
			{
				int index = boundaryMask->ComputeIndex(x, y, z);

				if (mPtr[index] == (int)CellType::Working){

					int pitchY = boundaryMask->PitchY();
					int pitchZ = boundaryMask->PitchZ();

					if (mPtr[index + 1] == (int)CellType::Dead
						|| mPtr[index - 1] == (int)CellType::Dead
						|| mPtr[index + pitchY] == (int)CellType::Dead
						|| mPtr[index - pitchY] == (int)CellType::Dead
						|| mPtr[index + pitchZ] == (int)CellType::Dead
						|| mPtr[index - pitchZ] == (int)CellType::Dead)
					{
						//����� � ������ [-0.5, 0.5]
						real rx = (real)x / (NX - 1) - 0.5;
						real ry = (real)y / (NY - 1) - 0.5;
						real rz = (real)z / (NZ - 1) - 0.5;
						real l = sqrt(rx*rx + ry*ry + rz*rz);

						bMPtr[index] = (int)BoundaryCondition::Neumann;
						//���� ��
						if (ry>0)
							neumannValues->Pointer()[index] = sin(acos(rz/l))*sin(-atan(ry/rx));
						else
							neumannValues->Pointer()[index] = sin(acos(rz / l))*sin(-(M_PI + atan(ry / rx)));
						neumannXValues->Pointer()[index] = rx / l;
						neumannYValues->Pointer()[index] = ry / l;
						neumannZValues->Pointer()[index] = rz / l;
						mPtr[index] = (int)CellType::Boundary;
					}
				}
			}


	real* nptr = neumannValues->Pointer();
	Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	f->Clear();

	//�������� ���
	CompressedIndices *boundary = CompressedIndices::Create(mask, &selector);
	CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
	CompressedValues<real> *dValues = CompressedValues<real>::Create(direchletValues, boundary);
	CompressedValues<real> *nValues = CompressedValues<real>::Create(neumannValues, boundary);
	CompressedValues<real> *nxValues = CompressedValues<real>::Create(neumannXValues, boundary);
	CompressedValues<real> *nyValues = CompressedValues<real>::Create(neumannYValues, boundary);
	CompressedValues<real> *nzValues = CompressedValues<real>::Create(neumannZValues, boundary);


	PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, dValues, nValues, nxValues, nyValues, nzValues, 1.0 / (NX - 1), 1e-5f);

	//Helper::GridPrint<real>(res);

	Grid <real>* rightAnswer = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	rightAnswer->Clear();
	Grid <real>* err = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	err->Clear();

	real* ptr = res->Pointer();
	real max = 0;
	for (size_t z = 0; z < NZ; z++)
		for (size_t y = 0; y < NY; y++)
			for (size_t x = 0; x < NX; x++){
				int index = res->ComputeIndex(x, y, z);
				if (mask->Pointer()[index] != CellType::Dead){
					real rx = (real)x / (NX - 1) - 0.5;
					real ry = (real)y / (NY - 1) - 0.5;
					real rz = (real)z / (NZ - 1) - 0.5;

					real r2 = rx*rx + ry*ry + rz*rz;
					real l = sqrt(r2);
					//*rx/sqrt(rx*rx+ry*ry) = cos(arctg(ry/rx))
					real val;
					if (ry>0)
						val = 0.25 / 2 / r2*sin(acos(rz / l))*sin(atan(ry / rx));
					else
						val = 0.25 / 2 / r2*sin(acos(rz / l))*sin(M_PI + atan(ry / rx));

					real rs = res->Pointer()[index];
					real resval = rs - val;

					if (abs(resval) > max)
						max = abs(resval);
					rightAnswer->Pointer()[index] = val;
					err->Pointer()[index] = resval;
				}
			}
	cout << max;
	//Helper::GridPrint<real>(rightAnswer);
	//Helper::GridPrint<real>(err);
	//Helper::GridSave<real>(err);
	getchar();

	*/
	size_t NX = 130, NY = 130, NZ = 130;
	PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);

	std::vector<std::pair<DeviceDescriptor, float> > devices;
	devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
	solver->Configure(devices);

	Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *direchletValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *neumannValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	neumannValues->Clear();
	Grid<real> *neumannXValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *neumannYValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *neumannZValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);

	for (size_t z = 0; z < NZ; z++)
		for (size_t y = 0; y < NY; y++)
			for (size_t x = 0; x < NX; x++)
			{
				bool isDirichlet = (x == 0) || (x == NX - 1) ||
					(y == 0) || (y == NY - 1) ||
					(z == 0) || (z == NZ - 1);

				int index = boundaryMask->ComputeIndex(x, y, z);

				real rx = (real)x / (NX - 1) - 0.5;
				real ry = (real)y / (NY - 1) - 0.5;
				real rz = (real)z / (NZ - 1) - 0.5;

				real r2 = rx*rx + ry*ry + rz*rz;
				real l = sqrt(r2);

				if (isDirichlet){
					boundaryMask->Pointer()[index] = (int)BoundaryCondition::Dirichlet;
					direchletValues->Pointer()[index] = 1000 * 0.25*0.25*0.25 / 2 / r2*rz / l;
					mask->Pointer()[index] = (int)CellType::Boundary;
				}
				else {
					//r=0.25
					if ((rx*rx) + (ry*ry) + (rz*rz) < 0.0625)
					{

						mask->Pointer()[index] = (int)CellType::Dead;
					}
					else {
						mask->Pointer()[index] = (int)CellType::Working;
					}

					boundaryMask->Pointer()[index] = 0;
					direchletValues->Pointer()[index] = 0.0f;
				}
			}

	int* bMPtr = boundaryMask->Pointer();
	int* mPtr = mask->Pointer();

	for (size_t z = 0; z < NZ; z++)
		for (size_t y = 0; y < NY; y++)
			for (size_t x = 0; x < NX; x++)
			{
				int index = boundaryMask->ComputeIndex(x, y, z);

				if (mPtr[index] == (int)CellType::Working){

					int pitchY = boundaryMask->PitchY();
					int pitchZ = boundaryMask->PitchZ();

					if (mPtr[index + 1] == (int)CellType::Dead
						|| mPtr[index - 1] == (int)CellType::Dead
						|| mPtr[index + pitchY] == (int)CellType::Dead
						|| mPtr[index - pitchY] == (int)CellType::Dead
						|| mPtr[index + pitchZ] == (int)CellType::Dead
						|| mPtr[index - pitchZ] == (int)CellType::Dead)
					{
						//����� � ������ [-0.5, 0.5]
						real rx = (real)x / (NX - 1) - 0.5;
						real ry = (real)y / (NY - 1) - 0.5;
						real rz = (real)z / (NZ - 1) - 0.5;
						real l = sqrt(rx*rx + ry*ry + rz*rz);

						bMPtr[index] = (int)BoundaryCondition::Neumann;
						//���� ��
						neumannValues->Pointer()[index] = -1000 * rz / l;
						neumannXValues->Pointer()[index] = rx / l;
						neumannYValues->Pointer()[index] = ry / l;
						neumannZValues->Pointer()[index] = rz / l;
						mPtr[index] = (int)CellType::Boundary;
					}
				}
			}

	real* nptr = neumannValues->Pointer();
	Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	f->Clear();
	
	CompressedIndices *boundary = CompressedIndices::Create(mask, &selector);
	CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
	CompressedValues<real> *dValues = CompressedValues<real>::Create(direchletValues, boundary);
	CompressedValues<real> *nValues = CompressedValues<real>::Create(neumannValues, boundary);
	CompressedValues<real> *nxValues = CompressedValues<real>::Create(neumannXValues, boundary);
	CompressedValues<real> *nyValues = CompressedValues<real>::Create(neumannYValues, boundary);
	CompressedValues<real> *nzValues = CompressedValues<real>::Create(neumannZValues, boundary);
	

	PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, dValues, nValues, nxValues, nyValues, nzValues, 1.0 / (NX-1), 1e-5f);

	Helper::GridSave<real>(res, "FullMultigrid.csv");

	Grid <real>* rightAnswer = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	rightAnswer->Clear();
	Grid <real>* err = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
	err->Clear();

	real* ptr = res->Pointer();
	real max = 0;
	real avgerr = 0;
	for (size_t z = 0; z < NZ; z++)
		for (size_t y = 0; y < NY; y++)
			for (size_t x = 0; x < NX; x++){
				int index = res->ComputeIndex(x, y, z);
				real rx = (real)x / (NX - 1) - 0.5;
				real ry = (real)y / (NY - 1) - 0.5;
				real rz = (real)z / (NZ - 1) - 0.5;

				real r2 = rx*rx + ry*ry + rz*rz;
				real l = sqrt(r2);
				//*rx/sqrt(rx*rx+ry*ry) = cos(arctg(ry/rx))
				real val = 1000 * 0.25*0.25*0.25 / 2 / r2*rz / l;
				real rs = res->Pointer()[index];
				real resval = rs - val;
				if (mask->Pointer()[index] != CellType::Dead)
				rightAnswer->Pointer()[index] = val;
				if (mask->Pointer()[index] == CellType::Working){
					if (abs(resval) > max)
						max = abs(resval);
					avgerr += resval*resval;
					err->Pointer()[index] = abs(resval);
				}
			}

	avgerr /= (NX*NY*NZ);
	avgerr = sqrt(avgerr);
	cout << avgerr <<endl;
	cout << max;

	//Helper::GridSave<real>(rightAnswer, "RightAnswer.csv");
	//Helper::GridPrint<real>(err);
/*
size_t NX = 130, NY = 130, NZ = 130;
PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);

std::vector<std::pair<DeviceDescriptor, float> > devices;
devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
solver->Configure(devices);

Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
Grid<real> *direchletValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
for (size_t z = 0; z < NZ; z++)
	for (size_t y = 0; y < NY; y++)
		for (size_t x = 0; x < NX; x++)
		{
			bool isBoundary = (x == 0) || (x == NX - 1) ||
				(y == 0) || (y == NY - 1) ||
				(z == 0) || (z == NZ - 1);

			int index = boundaryMask->ComputeIndex(x, y, z);
			boundaryMask->Pointer()[index] = isBoundary ? (int)BoundaryCondition::Dirichlet : 0;
			direchletValues->Pointer()[index] = isBoundary ? std::abs((float)x / (NX - 1) + (float)y / (NY - 1) + (float)z / (NZ - 1)) : 0.0f;
			mask->Pointer()[index] = isBoundary ? (int)CellType::Boundary : (int)CellType::Working;
		}

Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
f->Clear();

CompressedIndices *boundary = CompressedIndices::Create(mask, &selector);
CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
CompressedValues<real> *dValues = CompressedValues<real>::Create(direchletValues, boundary);

PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, dValues, dValues, dValues, dValues, dValues, 1.0 / NX, 0.01);


	Helper::GridSave<real>(res);
	getchar();
*//*
	try
	{
		printf("Temporary project for testing only. Will be removed after the \"Benchmark\" project creation.");

		size_t NX = 130, NY = 130, NZ = 130;
		PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);
	
		std::vector<std::pair<DeviceDescriptor, float> > devices;
		devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
		solver->Configure(devices);

		Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
		Grid<real> *boundaryValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
		Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
		for (size_t z = 0; z < NZ; z++)
			for (size_t y = 0; y < NY; y++)
				for (size_t x = 0; x < NX; x++)
				{
					bool isBoundary = (x == 0) || (x == NX - 1) ||
									  (y == 0) || (y == NY - 1) ||
									  (z == 0) || (z == NZ - 1);

					int index = boundaryMask->ComputeIndex(x, y, z);
					boundaryMask->Pointer()[index] = isBoundary ? 0 : (int)BoundaryCondition::Dirichlet;
					boundaryValues->Pointer()[index] = isBoundary ? std::abs(0.5f - (float)x / NX) : 0.0f;
					mask->Pointer()[index] = isBoundary ? (int)CellType::Boundary : (int)CellType::Working;
				}

		Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
		Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
		f->Clear();

		CompressedIndices *boundary = CompressedIndices::Create(boundaryMask, &selector);
		CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
		CompressedValues<real> *values = CompressedValues<real>::Create(boundaryValues, boundary);

		PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, values, values, values, values, values, 1.0 / NX, 1e-5f);

		printf("Performance information:");
		for (size_t i = 0; i < pInfo.Records().size(); i++)
		{
			const PerfRecord &rec = pInfo.Records()[i];

			printf("   %s: %lf seconds\n", rec.Description().c_str(), rec.ElapsedTime());
			printf("      %Performance: %lf %s\n", rec.PerformanceValue(), rec.PerformanceUnits().c_str());
			printf("      %Bandwidth:   %lf %s\n", rec.BandwidthValue(), rec.BandwidthUnits().c_str());
			printf("\n");
		}
		getchar();
		return 0;
	}
	catch (std::exception &ex)
	{
		printf("The following error has occured: %s\n", ex.what());
		return 42;
	}*/
}
