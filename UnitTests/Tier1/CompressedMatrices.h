
#pragma once

using namespace Microsoft::VisualStudio::TestTools::UnitTesting;

#include "Solver/All.h"

#include <map>

namespace UnitTests
{
	static inline bool IsNotZero(int val)
	{ return val != 0; }

	[TestClass]
	public ref class CompressedMatrices
	{
		private:
			static void UniTest(Grid<int> *matrix)
			{
				//Step 1. Generating compressed indices for nonzero elements.
				std::auto_ptr<CompressedIndices> _cIndices(CompressedIndices::Create(matrix, IsNotZero));
				CompressedIndices *cIndices = _cIndices.get();

				//Step 2. Generating list with nonzero values.
				std::auto_ptr<CompressedValues<int> > _cValues(CompressedValues<int>::Create(matrix, cIndices));
				CompressedValues<int> *cValues = _cValues.get();

				//Step 3. Checking correctness (the same amount of the same elements).
				std::map<int, size_t> dCounts;
				size_t dNonzero = 0;
				for (size_t z = 0; z < matrix->SizeZ(); z++)
					for (size_t y = 0; y < matrix->SizeY(); y++)
						for (size_t x = 0; x < matrix->SizeX(); x++)
						{
							size_t index = matrix->ComputeIndex(x, y, z);
							int val = matrix->Pointer()[index];
							if (val != 0)
							{
								dCounts[val] += 1;
								dNonzero += 1;
							}
						}

				std::map<int, size_t> cCounts;
				for (size_t i = 0; i < cValues->Count(); i++)
				{
					cCounts[cValues->Pointer()[i]] += 1;
				}

				if (cValues->Count() != cIndices->Count())
				{ throw gcnew System::ApplicationException("Number of the compressed indices and compressed values are not the same"); } 

				if (cValues->Count() != dNonzero)
				{ throw gcnew System::ApplicationException("Dense matrix has another number of the nonzero elements"); }

				for (std::map<int, size_t>::iterator it = dCounts.begin();
					it != dCounts.end();
					it++)
				{
					if (cCounts[it->first] != it->second)
						throw gcnew System::ApplicationException("Some values from dense matrix were lost or duplicated");
				}

			}

			static void RandTest(size_t sizeX, size_t sizeY, size_t sizeZ)
			{
				std::auto_ptr<Grid<int> > matrix(Grid<int>::Create(MemoryType::Host, sizeX, sizeY, sizeZ));

				for (size_t z = 0; z < matrix->SizeZ(); z++)
					for (size_t y = 0; y < matrix->SizeY(); y++)
						for (size_t x = 0; x < matrix->SizeX(); x++)
						{
							size_t index = matrix->ComputeIndex(x, y, z);

							int r = rand() & 0xFF;

							matrix->Pointer()[index] = (r & 1) == 0 ? 0 : (r >> 1);
						}

				UniTest(matrix.get());
			}

		public:
			[TestMethod]
			void Rand_3x3x3()
			{ RandTest(3, 3, 3); }

			[TestMethod]
			void Rand_8x8x8()
			{ RandTest(8, 8, 8); }

			/*[TestMethod]
			void Rand_10x2x5()
			{ RandTest(10, 2, 5); }

			[TestMethod]
			void Rand_1x16x1()
			{ RandTest(1, 16, 1); }*/
	};
}
