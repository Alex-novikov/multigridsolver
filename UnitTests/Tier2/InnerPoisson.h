
#pragma once

using namespace Microsoft::VisualStudio::TestTools::UnitTesting;


#include "Solver/All.h"
#include <map>

namespace UnitTests
{
	bool selector(int value){
		if (value == (int)CellType::Boundary)
			return true;
		return false;
	}

	[TestClass]
	public ref class InnerPoisson
	{

	public:
		[TestMethod]
		void ZeroBounds()
		{
			size_t NX = 130, NY = 130, NZ = 130;
			PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);

			std::vector<std::pair<DeviceDescriptor, float> > devices;
			devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
			solver->Configure(devices);

			Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *direchletValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++)
					{
						bool isBoundary = (x == 0) || (x == NX - 1) ||
							(y == 0) || (y == NY - 1) ||
							(z == 0) || (z == NZ - 1);

						int index = boundaryMask->ComputeIndex(x, y, z);
						boundaryMask->Pointer()[index] = isBoundary ? (int)BoundaryCondition::Dirichlet : 0;
						direchletValues->Pointer()[index] = 0;
						mask->Pointer()[index] = isBoundary ? (int)CellType::Boundary : (int)CellType::Working;
					}

			Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			f->Clear();

			CompressedIndices *boundary = CompressedIndices::Create(mask, &selector);
			CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
			CompressedValues<real> *dValues = CompressedValues<real>::Create(direchletValues, boundary);

			PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, dValues, dValues, dValues, dValues, dValues, 1.0 / NX, 1e-5f);

			for (int i = 0; i < NX*NY*NZ; i++){
				if (abs(res->Pointer()[i]) > 1.0 / NX)
					throw gcnew System::ApplicationException("Bad solver");
			}
		}

		[TestMethod]
		void DirichletProblem()
		{
			size_t NX = 34, NY = 34, NZ = 34;
			PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);

			std::vector<std::pair<DeviceDescriptor, float> > devices;
			devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
			solver->Configure(devices);

			Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *direchletValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++)
					{
						bool isBoundary = (x == 0) || (x == NX - 1) ||
							(y == 0) || (y == NY - 1) ||
							(z == 0) || (z == NZ - 1);

						int index = boundaryMask->ComputeIndex(x, y, z);
						boundaryMask->Pointer()[index] = isBoundary ? (int)BoundaryCondition::Dirichlet : 0;
						direchletValues->Pointer()[index] = isBoundary ? std::abs((float)x / (NX - 1) + (float)y / (NY - 1) + (float)z / (NZ - 1)) : 0.0f;
						mask->Pointer()[index] = isBoundary ? (int)CellType::Boundary : (int)CellType::Working;
					}

			Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			f->Clear();

			CompressedIndices *boundary = CompressedIndices::Create(mask, &selector);
			CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
			CompressedValues<real> *dValues = CompressedValues<real>::Create(direchletValues, boundary);

			PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, dValues, dValues, dValues, dValues, dValues, 1.0 / NX, 1e-5f);

			real* ptr = res->Pointer();
			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++){
						int index = res->ComputeIndex(x, y, z);
						if (std::abs(res->Pointer()[index] - float(x) / (NX - 1) - float(y) / (NY - 1) - float(z) / (NZ - 1)) > 1.0){
							System::Console::WriteLine(x);
							System::Console::WriteLine(y);
							System::Console::WriteLine(z);
							System::Console::WriteLine(std::abs(res->Pointer()[index] - float(x) / (NX - 1) - float(y) / (NY - 1) - float(z) / (NZ - 1)));
							throw gcnew System::ApplicationException("Bad solver");
						}
					}
		}

		[TestMethod]
		void NeumannProblem()
		{
			size_t NX = 130, NY = 130, NZ = 130;
			PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);

			std::vector<std::pair<DeviceDescriptor, float> > devices;
			devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
			solver->Configure(devices);

			Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *direchletValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *neumannValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			neumannValues->Clear();
			Grid<real> *neumannXValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *neumannYValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *neumannZValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);

			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++)
					{
						bool isDirichlet = (x == 0) || (x == NX - 1) ||
							(y == 0) || (y == NY - 1) ||
							(z == 0) || (z == NZ - 1);

						int index = boundaryMask->ComputeIndex(x, y, z);

						real rx = (real)x / (NX - 1) - 0.5;
						real ry = (real)y / (NY - 1) - 0.5;
						real rz = (real)z / (NZ - 1) - 0.5;

						real r2 = rx*rx + ry*ry + rz*rz;

						if (isDirichlet){
							boundaryMask->Pointer()[index] = (int)BoundaryCondition::Dirichlet;
							direchletValues->Pointer()[index] = 1000 * 0.25*0.25*0.25 / 2 / r2*rx / sqrt(rx*rx + ry*ry);
							mask->Pointer()[index] = (int)CellType::Boundary;
						}
						else {
							//r=0.25
							if ((rx*rx) + (ry*ry) + (rz*rz) < 0.0625)
							{

								mask->Pointer()[index] = (int)CellType::Dead;
							}
							else {
								mask->Pointer()[index] = (int)CellType::Working;
							}

							boundaryMask->Pointer()[index] = 0;
							direchletValues->Pointer()[index] = 0.0f;
						}
					}

			int* bMPtr = boundaryMask->Pointer();
			int* mPtr = mask->Pointer();

			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++)
					{
						int index = boundaryMask->ComputeIndex(x, y, z);

						if (mPtr[index] == (int)CellType::Working){

							int pitchY = boundaryMask->PitchY();
							int pitchZ = boundaryMask->PitchZ();

							if (mPtr[index + 1] == (int)CellType::Dead
								|| mPtr[index - 1] == (int)CellType::Dead
								|| mPtr[index + pitchY] == (int)CellType::Dead
								|| mPtr[index - pitchY] == (int)CellType::Dead
								|| mPtr[index + pitchZ] == (int)CellType::Dead
								|| mPtr[index - pitchZ] == (int)CellType::Dead)
							{
								//����� � ������ [-0.5, 0.5]
								real rx = (real)x / (NX - 1) - 0.5;
								real ry = (real)y / (NY - 1) - 0.5;
								real rz = (real)z / (NZ - 1) - 0.5;
								real l = sqrt(rx*rx + ry*ry + rz*rz);

								bMPtr[index] = (int)BoundaryCondition::Neumann;
								//���� ��
								neumannValues->Pointer()[index] = 1000 * rx / sqrt(rx*rx + ry*ry);
								neumannXValues->Pointer()[index] = rx / l;
								neumannYValues->Pointer()[index] = ry / l;
								neumannZValues->Pointer()[index] = rz / l;
								mPtr[index] = (int)CellType::Boundary;
							}
						}
					}


			real* nptr = neumannValues->Pointer();
			Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			f->Clear();

			//�������� ���
			CompressedIndices *boundary = CompressedIndices::Create(mask, &selector);
			CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
			CompressedValues<real> *dValues = CompressedValues<real>::Create(direchletValues, boundary);
			CompressedValues<real> *nValues = CompressedValues<real>::Create(neumannValues, boundary);
			CompressedValues<real> *nxValues = CompressedValues<real>::Create(neumannXValues, boundary);
			CompressedValues<real> *nyValues = CompressedValues<real>::Create(neumannYValues, boundary);
			CompressedValues<real> *nzValues = CompressedValues<real>::Create(neumannZValues, boundary);

			PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, dValues, nValues, nxValues, nyValues, nzValues, 1.0 / NX, 1e-5f);

			real* ptr = res->Pointer();
			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++){
						int index = res->ComputeIndex(x, y, z);

						real rx = (real)x / (NX - 1) - 0.5;
						real ry = (real)y / (NY - 1) - 0.5;
						real rz = (real)z / (NZ - 1) - 0.5;

						real r2 = rx*rx + ry*ry + rz*rz;

						real val = 1000 * 0.25*0.25*0.25 / 2 / r2*rx / sqrt(rx*rx + ry*ry);
						real rs = res->Pointer()[index];
						real resval = rs - val;
						if (mask->Pointer()[index] == CellType::Working){
							if (std::abs(resval) > 150){
								System::Console::WriteLine(x);
								System::Console::WriteLine(y);
								System::Console::WriteLine(z);
								System::Console::WriteLine("nado");
								System::Console::WriteLine(val);
								System::Console::WriteLine("est'");
								System::Console::WriteLine(res->Pointer()[index]);
								throw gcnew System::ApplicationException("Bad solver");
							}
						}
					}
		}
	};
}