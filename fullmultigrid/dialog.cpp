#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog){
    ui->setupUi(this);
}

Dialog::~Dialog(){
    //delete ui;
}

void Dialog::on_buttonBox_clicked(QAbstractButton *button){
    this->x = this->ui->xInput->text().toInt();
    this->y = this->ui->yInput->text().toInt();
    this->t = this->ui->tInput->text().toInt();
    this->alpha = this->ui->alphaInput->text().toDouble();

    this->close();
}
