#ifndef STATICCONSTS_H
#define STATICCONSTS_H

#include <QMatrix4x4>;

// матрица сдвига текстурных координат
static const QMatrix4x4 bias(
        0.5f, 0.0f, 0.0f, 0.5f,
        0.0f, 0.5f, 0.0f, 0.5f,
        0.0f, 0.0f, 0.5f, 0.5f,
        0.0f, 0.0f, 0.0f, 1.0f
        );

static const double lineData[18] ={
        0.0f, 0.0f, 0.0f, 0.0f, 1.5f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.5f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.5f
        };

const float degToRad = float( M_PI / 180.0 );


#endif // STATICCONSTS_H
