
#include <QtWidgets>
#include <QtOpenGL>

#include <math.h>

#include "glwidget.h"

#include "waterscene.h"

#include <QCoreApplication>
#include <QKeyEvent>
#include <QOpenGLContext>
#include <QTimer>

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

GLWidget::GLWidget(double***data,int x, int y, int t, QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent),
    m_scene( new WaterScene( data, x, y, t, this ) ),
    m_leftButtonPressed( false ){
    time = 0;
    qtGreen = QColor::fromCmykF(0.40, 0.0, 1.0, 0.0);
    qtPurple = QColor::fromCmykF(0.39, 0.39, 0.0, 0.0);

    // Specify the format we wish to use
   /* QGLFormat format;
    format.setDepthBufferSize( 24 );
    format.setMajorVersion( 4 );
    format.setMinorVersion( 3 );
    format.setSamples( 4 );
    format.setProfile( QGLFormat::CoreProfile );

    // Create an OpenGL context
    this->setFormat(format);
    this->create();

    // Setup our scene
    this->makeCurrent();
    m_scene->setContext( m_context );
    // This timer drives the scene updates
    */QTimer* timer = new QTimer( this );
    connect( timer, SIGNAL( timeout() ), this, SLOT( updateScene() ) );
    timer->start( 16 );
}

GLWidget::~GLWidget(){
    //delete m_scene;
}

QSize GLWidget::minimumSizeHint() const {
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const {
    return QSize(400, 400);
}

static void qNormalizeAngle(int &angle){
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

void GLWidget::setTime(int time){
    qDebug() << time;
    static_cast<WaterScene*>(m_scene)->setTime(time);
    emit timeChanged(time);
    updateGL();
}



void GLWidget::initializeGL() {
    glEnable(GL_MULTISAMPLE);
    makeCurrent();
    m_scene->initialize();
    m_time.start();
}

void GLWidget::paintGL(){

    makeCurrent();
    this->setFocus();
    // Рендерим сцену
    m_scene->render();

    // Swap front/back buffers
    if (!static_cast<QWindow*>(this->parent())->isExposed())
            swapBuffers();
}

void GLWidget::resizeGL(int width, int height){
    makeCurrent();
    qDebug() << width;
    m_scene->resize( width, height );
}

void GLWidget::updateScene() {
    float time = m_time.elapsed() / 1000.0f;
    m_scene->update( time );
    paintGL();
}
void GLWidget::keyPressEvent( QKeyEvent* e ) {
    const float speed = 150.0f;
    WaterScene* scene = static_cast<WaterScene*>( m_scene );
    switch ( e->key() )
    {
    case Qt::Key_Escape:
        QCoreApplication::instance()->quit();
        break;

    case Qt::Key_D:
        scene->setSideSpeed( speed );
        break;

    case Qt::Key_A:
        scene->setSideSpeed( -speed );
        break;

    case Qt::Key_W:
        scene->setForwardSpeed( speed );
        break;

    case Qt::Key_S:
        scene->setForwardSpeed( -speed );
        break;

    case Qt::Key_PageUp:
        scene->setVerticalSpeed( speed );
        break;

    case Qt::Key_PageDown:
        scene->setVerticalSpeed( -speed );
        break;

    case Qt::Key_Shift:
        scene->setViewCenterFixed( true );
        break;

    case Qt::Key_BracketLeft:
        scene->setSunAngle( scene->sunAngle() - 2);
        break;

    case Qt::Key_BracketRight:
        scene->setSunAngle( scene->sunAngle() + 2 );
        break;

    case Qt::Key_Comma:
        scene->setScreenSpaceError( scene->screenSpaceError() + 0.1 );
        break;

    case Qt::Key_Period:
        scene->setScreenSpaceError( scene->screenSpaceError() - 0.1 );
        break;

    case Qt::Key_F1:
        scene->setDisplayMode( WaterScene::TexturedAndLit );
        break;

    case Qt::Key_F2:
        scene->setDisplayMode( WaterScene::SimpleWireFrame );
        break;

    case Qt::Key_F3:
        scene->setDisplayMode( WaterScene::WorldHeight );
        break;

    case Qt::Key_F4:
        scene->setDisplayMode( WaterScene::WorldNormals );
        break;

    case Qt::Key_F8:
        scene->setDisplayMode( WaterScene::LightingFactors );
        break;

    default:
        QGLWidget::keyPressEvent( e );
    }
}
void GLWidget::keyReleaseEvent( QKeyEvent* e ){
    WaterScene* scene = static_cast<WaterScene*>( m_scene );
    switch ( e->key() ){
    case Qt::Key_D:
    case Qt::Key_A:
        scene->setSideSpeed( 0.0f );
        break;

    case Qt::Key_W:
    case Qt::Key_S:
        scene->setForwardSpeed( 0.0f );
        break;

    case Qt::Key_PageUp:
    case Qt::Key_PageDown:
        scene->setVerticalSpeed( 0.0f );
        break;

    case Qt::Key_Shift:
        scene->setViewCenterFixed( false );
        break;

    default:
        QGLWidget::keyReleaseEvent( e );
    }
}


void GLWidget::mousePressEvent(QMouseEvent *e){
    if ( e->button() == Qt::LeftButton )
    {
        m_leftButtonPressed = true;
        m_pos = m_prevPos = e->pos();
    }
    QGLWidget::mousePressEvent( e );
}
void GLWidget::mouseReleaseEvent( QMouseEvent* e ){
    if ( e->button() == Qt::LeftButton )
        m_leftButtonPressed = false;
    QGLWidget::mouseReleaseEvent( e );
}

void GLWidget::mouseMoveEvent(QMouseEvent *e){
    if ( m_leftButtonPressed ){
        m_pos = e->pos();
        float dx = 0.2f * ( m_pos.x() - m_prevPos.x() );
        float dy = -0.2f * ( m_pos.y() - m_prevPos.y() );
        m_prevPos = m_pos;

        WaterScene* scene = static_cast<WaterScene*>( m_scene );
        scene->pan( dx );
        scene->tilt( dy );
    }

    QGLWidget::mouseMoveEvent( e );
}

