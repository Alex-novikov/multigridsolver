#version 330 core

uniform sampler2D renderedTexture;
uniform sampler2D depthMap;

in vec2 texcoord;
out vec4 result_color;

#define PI  3.14159265

float width = 1366; //немножко 
float height = 768; //плохо

vec2 texel = vec2(1.0/width,1.0/height);

int samples = 3;
int rings = 5; 

vec2 focus = vec2(0.5,0.5); // куда будем фокусироваться
float range = 15.0; //фокусное расстояние
float maxblur = 1.0; //максимальное размытие

float threshold = 0.5; //порог;
float gain = 5.0; //гейн;

float bias = 0.4; 
float fringe = 0.5; 

float namount = 0.0001; 

float dbsize = 2.0; 
float bdepth(vec2 coords) {
        float d = 0.0;
        float kernel[9];
        vec2 offset[9];

        vec2 wh = vec2(texel.x, texel.y) * dbsize;

        offset[0] = vec2(-wh.x,-wh.y);
        offset[1] = vec2( 0.0, -wh.y);
        offset[2] = vec2( wh.x -wh.y);

        offset[3] = vec2(-wh.x,  0.0);
        offset[4] = vec2( 0.0,   0.0);
        offset[5] = vec2( wh.x,  0.0);

        offset[6] = vec2(-wh.x, wh.y);
        offset[7] = vec2( 0.0,  wh.y);
        offset[8] = vec2( wh.x, wh.y);

        kernel[0] = 1.0/16.0;   kernel[1] = 2.0/16.0;   kernel[2] = 1.0/16.0;
        kernel[3] = 2.0/16.0;   kernel[4] = 4.0/16.0;   kernel[5] = 2.0/16.0;
        kernel[6] = 1.0/16.0;   kernel[7] = 2.0/16.0;   kernel[8] = 1.0/16.0;


        for( int i=0; i<9; i++ ) {
                float tmp = texture2D(depthMap, coords + offset[i]).r;
                d += tmp * kernel[i];
        }

        return d;
}


vec3 color(vec2 coords,float blur) {
        vec3 col = vec3(0.0);

        col.r = texture2D(renderedTexture,coords + vec2(0.0,1.0)*texel*fringe*blur).r;
        col.g = texture2D(renderedTexture,coords + vec2(-0.866,-0.5)*texel*fringe*blur).g;
        col.b = texture2D(renderedTexture,coords + vec2(0.866,-0.5)*texel*fringe*blur).b;

        vec3 lumcoeff = vec3(0.299,0.587,0.114);
        float lum = dot(col.rgb, lumcoeff);
        float thresh = max((lum-threshold)*gain, 0.0);
        return col+mix(vec3(0.0),col,thresh*blur);
}

vec2 rand(in vec2 coord) { 
        float noiseX = clamp(fract(sin(dot(coord ,vec2(12.9898,78.233))) * 43758.5453),0.0,1.0)*2.0-1.0;
        float noiseY = clamp(fract(sin(dot(coord ,vec2(12.9898,78.233)*2.0)) * 43758.5453),0.0,1.0)*2.0-1.0;
        return vec2(noiseX,noiseY);
}

void main(){

        float depth = texture2D(depthMap, texcoord).x;
        float blur = 0.0;

        depth = bdepth(texcoord);

        float fDepth = texture2D(depthMap,focus).x;
        blur = clamp((abs(depth - fDepth)/range)*100.0,-maxblur,maxblur);

        vec2 noise = rand(texcoord)*namount*blur; //генерация шума

        float w = (1.0/width)*blur+noise.x;
        float h = (1.0/height)*blur+noise.y;

        vec3 col = texture2D(renderedTexture, texcoord).rgb;
        float s = 1.0;

        int ringsamples;

        for (int i = 1; i <= rings; i += 1){
                ringsamples = i * samples;

                for (int j = 0 ; j < ringsamples ; j += 1) {
                        float step = PI*2.0 / float(ringsamples);
                        float pw = (cos(float(j)*step)*float(i));
                        float ph = (sin(float(j)*step)*float(i));
                        float p = 1.0;
                        col += color(texcoord + vec2(pw*w,ph*h),blur)*mix(1.0,(float(i))/(float(rings)),bias)*p;
                        s += 1.0*mix(1.0,(float(i))/(float(rings)),bias)*p;
                }
        }


        col /= s;

        result_color = vec4(col,1.0);
}
