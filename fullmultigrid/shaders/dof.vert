#version 330 core

in vec3 position;
in vec2 texCoords;

// параметры для фрагментного шейдера
out vec2 texcoord;

void main(void){
        // передаем текстурные координаты
        texcoord = texCoords;

        // переводим координаты вершины в однородные
        gl_Position = vec4(position, 1.0);
}
