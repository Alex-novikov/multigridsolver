#include "waterscene.h"
#include "camera.h"

#include <QImage>
#include <QGLWidget>
#include <QOpenGLContext>
#include <QGLFunctions>

#define _USE_MATH_DEFINES

#define FAR_CLIPPLANE 100.0f
#include <math.h>

float Mix(float D1,float D2,float d){
    return (D1*d+D2*(1-d));
}

WaterScene::WaterScene( double ***data, int x, int y, int t, QObject* parent )
    : AbstractScene( parent ),
      m_camera( new Camera( this ) ),
      sunCamera( new Camera( this ) ),
      m_v(),
      m_viewCenterFixed( true ),
      m_panAngle( 0.0f ),
      m_tiltAngle( 0.0f ),
      m_screenSpaceError( 12.0f ),
      m_modelMatrix(),
      m_horizontalScale( 500.0f ),
      m_verticalScale( 20.0f ),
      m_sunTheta( 60.0f ),
      m_time( 0.0f ),
      m_metersToUnits( 0.05f ), // 500 units == 10 km => 0.05 units/m
      m_displayMode( SimpleWireFrame ),
      m_displayModeSubroutines( DisplayModeCount ),
      m_funcs( 0 ){
    x+=2;y+=2;
    m_modelMatrix.setToIdentity();
    time = 0;
    count = 2*(x-1)*(y-1);
    this->data = new double*[t];
    for(int k = 0; k < t; k++){
        this->data[k] = new double[3*3*count];
        for(int j = 0; j < y-1; j++)
            for(int i = 0; i < x-1; i++){
                this->data[k][18*(i+j*(x-1))+1] = data[i][j][k];
                this->data[k][18*(i+j*(x-1))] = 1.0/x*i;
                this->data[k][18*(i+j*(x-1))+2] = 1.0/y*j;
                this->data[k][18*(i+j*(x-1))+4] = data[i+1][j][k];
                this->data[k][18*(i+j*(x-1))+3] = 1.0/x*(i+1);
                this->data[k][18*(i+j*(x-1))+5] = 1.0/y*j;
                this->data[k][18*(i+j*(x-1))+7] = data[i][j+1][k];
                this->data[k][18*(i+j*(x-1))+6] = 1.0/x*i;
                this->data[k][18*(i+j*(x-1))+8] = 1.0/y*(j+1);
                this->data[k][18*(i+j*(x-1))+10] = data[i][j+1][k];
                this->data[k][18*(i+j*(x-1))+9] = 1.0/x*i;
                this->data[k][18*(i+j*(x-1))+11] = 1.0/y*(j+1);
                this->data[k][18*(i+j*(x-1))+13] = data[i+1][j+1][k];
                this->data[k][18*(i+j*(x-1))+12] = 1.0/x*(i+1);
                this->data[k][18*(i+j*(x-1))+14] = 1.0/y*(j+1);
                this->data[k][18*(i+j*(x-1))+16] = data[i+1][j][k];
                this->data[k][18*(i+j*(x-1))+15] = 1.0/x*(i+1);
                this->data[k][18*(i+j*(x-1))+17] = 1.0/y*j;
            }
    }
    double max = 0;

    // Initialize the camera position and orientation
    m_camera->setPosition(  QVector3D( 0.7f, 10.0f, 0.7f ) );
    m_camera->setViewCenter( QVector3D( 0.5f , 0.0f, 0.5f  ) );
    m_camera->setUpVector( QVector3D( 0.0f, -1.0f, 0.0f ) );

}

void WaterScene::initialize() {
    m_funcs = (static_cast<QGLWidget*>(this->parent()))->context()->functions();
    if ( !m_funcs ){
        qFatal("Requires OpenGL >= 3.3");
        exit( 1 );
    }
    m_funcs->initializeGLFunctions();

    prepareShaders();
    prepareVertexArrayObject();

    // Enable depth testing
    glEnable( GL_DEPTH_TEST );
    glDepthFunc(GL_LEQUAL);
    glDisable( GL_CULL_FACE );


    glClearColor( 0.65f, 0.77f, 1.0f, 1.0f );
    glClearDepth(1.0f);


}

void WaterScene::update( float t ){
    m_modelMatrix.setToIdentity();
    m_modelMatrix.rotate(90, -1.0, 0, 0);
    m_modelMatrix.rotate(180, 0, 0, 1);
    // Store the time
    const float dt = t - m_time;
    m_time = t;
    // Update the camera position and orientation
    Camera::CameraTranslationOption option = m_viewCenterFixed
            ? Camera::DontTranslateViewCenter
            : Camera::TranslateViewCenter;
    m_camera->translate( m_v * dt * m_metersToUnits, option );

    if ( !qFuzzyIsNull( m_panAngle ) )
    {
        m_camera->pan( m_panAngle, QVector3D( 0.0f, 1.0f, 0.0f ) );
        m_panAngle = 0.0f;
    }

    if ( !qFuzzyIsNull( m_tiltAngle ) )
    {
        m_camera->tilt( m_tiltAngle );
        m_tiltAngle = 0.0f;
    }
}

void WaterScene::render(){

    QOpenGLShaderProgramPtr shader = this->shader[0];

    // Pass in the usual transformation matrices
    shader->bind();
    QMatrix4x4 viewMatrix = m_camera->viewMatrix();
    QMatrix4x4 modelViewMatrix = viewMatrix * m_modelMatrix;
    QMatrix3x3 worldNormalMatrix = m_modelMatrix.normalMatrix();
    QMatrix3x3 normalMatrix = modelViewMatrix.normalMatrix();
    QMatrix4x4 mvp = m_camera->projectionMatrix() * modelViewMatrix;
    shader->setUniformValue( "modelMatrix", m_modelMatrix );
    shader->setUniformValue( "modelViewMatrix", modelViewMatrix );
    shader->setUniformValue( "worldNormalMatrix", worldNormalMatrix );
    shader->setUniformValue( "normalMatrix", normalMatrix );
    shader->setUniformValue( "modelViewProjectionMatrix", mvp );

    //Отправляем в шейдер наше местоположение
    shader->setUniformValue("viewPosition", m_camera->position());
    shader->setUniformValue( "modelViewProjectionMatrix", mvp );

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    QOpenGLVertexArrayObject::Binder binder0( &m_plotVAO );
    glDrawArrays(GL_TRIANGLES, 0, 3*count);

    QOpenGLVertexArrayObject::Binder binder1( &m_linesVAO );
    glDrawArrays(GL_LINES, 0, 18);
}

void WaterScene::resize( int w, int h ){
    // Make sure the viewport covers the entire window
    glViewport( 0, 0, w, h );

    m_viewportSize = QVector2D( float( w ), float( h ) );

    // Update the projection matrix
    float aspect = static_cast<float>( w ) / static_cast<float>( h );
    m_camera->setPerspectiveProjection( 25.0f, aspect, 0.1f, FAR_CLIPPLANE );
    // Update the viewport matrix
    float w2 = w / 2.0f;
    float h2 = h / 2.0f;
    m_viewportMatrix.setToIdentity();
    m_viewportMatrix.setColumn( 0, QVector4D( w2, 0.0f, 0.0f, 0.0f ) );
    m_viewportMatrix.setColumn( 1, QVector4D( 0.0f, h2, 0.0f, 0.0f ) );
    m_viewportMatrix.setColumn( 2, QVector4D( 0.0f, 0.0f, 1.0f, 0.0f ) );
    m_viewportMatrix.setColumn( 3, QVector4D( w2, h2, 0.0f, 1.0f ) );
}

void WaterScene::prepareShaders(){

    shader.push_back(QOpenGLShaderProgramPtr(new QOpenGLShaderProgram));
    if ( !shader[0]->addShaderFromSourceFile( QOpenGLShader::Vertex, ":/shaders/lightning.vert" ) )
        qCritical() << QObject::tr( "Could not compile vertex shader. Log:" ) << shader[0]->log();
    if ( !shader[0]->addShaderFromSourceFile( QOpenGLShader::Fragment, ":/shaders/lightning.frag" ) )
        qCritical() << QObject::tr( "Could not compile fragment shader. Log:" ) << shader[0]->log();
    if ( !shader[0]->link() )
        qCritical() << QObject::tr( "Could not link shader program. Log:" ) << shader[0]->log();



}

void WaterScene::prepareTextures(){

}

void WaterScene::prepareFrameBuffers(  ) {

}

void WaterScene::prepareVertexArrayObject(){

    // Создадим VAO для графика
    m_plotVAO.create();
    {
        QOpenGLVertexArrayObject::Binder binder( &m_plotVAO );
        QOpenGLShaderProgramPtr shader = this->shader[0];
        shader->bind();
        m_plotVBO.create();
        m_plotVBO.bind();
        m_plotVBO.setUsagePattern(QOpenGLBuffer::StaticDraw);

        m_plotVBO.allocate(data[0], 3*3*count*sizeof(double));
        shader->enableAttributeArray("position");
        shader->setAttributeArray("position", GL_DOUBLE, 0, 3, 3*sizeof(double));
    }

    m_linesVAO.create();
    {
        QOpenGLVertexArrayObject::Binder binder( &m_linesVAO );
        QOpenGLShaderProgramPtr shader = this->shader[0];
        shader->bind();
        m_linesVBO.create();
        m_linesVBO.bind();
        m_linesVBO.setUsagePattern(QOpenGLBuffer::StaticDraw);
        m_linesVBO.allocate(&lineData, 18*sizeof(double));
        shader->enableAttributeArray("position");
        shader->setAttributeArray("position", GL_DOUBLE, 0, 3, 3*sizeof(double));
    }
}

void WaterScene::setTime(int time){
    QOpenGLVertexArrayObject::Binder binder( &m_plotVAO );
    QOpenGLShaderProgramPtr shader = this->shader[0];
    shader->bind();
    m_plotVBO.create();
    m_plotVBO.bind();
    m_plotVBO.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_plotVBO.allocate(data[(int)time], 3*3*count*sizeof(double));
    shader->enableAttributeArray("position");
    shader->setAttributeArray("position", GL_DOUBLE, 0, 3, 3*sizeof(double));
}
