#ifndef TERRAINTESSELLATIONSCENE_H
#define TERRAINTESSELLATIONSCENE_H

#include "abstractscene.h"
#include "material.h"
#include "staticConsts.h"

#include <QGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLDebugLogger>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QMatrix4x4>
#include <QStringList>
#include <QVector>
#include <QImageWriter>
#include <QDebug>

#include <QOpenGLFramebufferObject>

typedef QSharedPointer<QOpenGLShaderProgram> QOpenGLShaderProgramPtr;

class Camera;

class QOpenGLFunctions_3_3;

float Mix(float D1,float D2,float d);

class WaterScene : public AbstractScene {
    Q_OBJECT

public:
    WaterScene( double *** data, int x, int y, int t, QObject* parent = 0 );

    virtual void initialize();
    virtual void update( float t );
    virtual void render();
    virtual void resize( int w, int h );


    // Camera motion control
    void setSideSpeed( float vx ) { m_v.setX( vx ); }
    void setVerticalSpeed( float vy ) { m_v.setY( vy ); }
    void setForwardSpeed( float vz ) { m_v.setZ( vz ); }
    void setViewCenterFixed( bool b ) { m_viewCenterFixed = b; }

    // Camera orientation control
    void pan( float angle ) { m_panAngle = angle; }
    void tilt( float angle ) { m_tiltAngle = angle; }

    // Terrain scales
    void setTime(int time);

    // Sun position
    void setSunAngle( float sunAngle ) {

        if ((sunAngle < 180) && (sunAngle>0))
            m_sunTheta = sunAngle;
        qDebug() << m_sunTheta; }
    float sunAngle() const { return m_sunTheta; }

    // Screen space error - tessellation control
    void setScreenSpaceError( float error ) { m_screenSpaceError = error; qDebug() << error; }
    float screenSpaceError() const { return m_screenSpaceError; }

    enum DisplayMode {
        SimpleWireFrame = 0,
        WorldHeight,
        WorldNormals,
        LightingFactors,
        TexturedAndLit,
        DisplayModeCount
    };

    void setDisplayMode( DisplayMode displayMode ) { m_displayMode = displayMode; }
    DisplayMode displayMode() const { return m_displayMode; }

private:

    int time,count;
    double ** data;
    GLuint sampler;
    void prepareShaders();
    void prepareTextures();
    void prepareFrameBuffers();
    void prepareVertexArrayObject();

    Camera* m_camera,* sunCamera;
    QVector3D m_v;
    bool m_viewCenterFixed;
    float m_panAngle;
    float m_tiltAngle;

    QMatrix4x4 m_viewportMatrix;
    QVector2D m_viewportSize;

    // The terrain "object"
    QOpenGLVertexArrayObject m_plotVAO, m_linesVAO;
    QOpenGLBuffer m_plotVBO, m_linesVBO;
    int m_patchCount;
    float m_screenSpaceError;

    // Terrain rendering controls
    QMatrix4x4 m_modelMatrix;
    float m_horizontalScale;
    float m_verticalScale;

    // Angle of sun. 0 is directly overhead, 90 to the East, -90 to the West
    float m_sunTheta;

    float m_time;
    const float m_metersToUnits;

    DisplayMode m_displayMode;
    QStringList m_displayModeNames;
    QVector<GLuint> m_displayModeSubroutines;

    QGLFunctions* m_funcs;
    QSize m_heightMapSize;

    //Тестура воды
    Texture waterMap;
    //Разные шейдеры;
    QVector<QOpenGLShaderProgramPtr> shader;
};

#endif // TERRAINTESSELLATIONSCENE_H
